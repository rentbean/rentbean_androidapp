package com.food.profile.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class EditUserProfileRequestModel {
    private String uid;
    private String name;
    private String mobile;
    private String dob;


    public EditUserProfileRequestModel(String uid, String name, String mobile, String dob) {
        this.uid = uid;
        this.name = name;
        this.mobile = mobile;
        this.dob = dob;

    }
}
