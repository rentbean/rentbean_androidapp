/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;


import com.food.core.common.PDHShopFlowCallback;


/**
 * Singleton class to keep track of main app context
 */
public class ShopKitApp {

    private static ShopKitApp instance = null;
    private Context mContext = null;
    private Activity mActivity = null;
    public FragmentActivity mFragmentActivity = null;
    public String VZWCutomerAgreement = "";
    public int mvmActivityResId = 0;
    private PDHShopFlowCallback shopFlowCallback = null;
    private static String TAG = "ShopKitApp";

    protected ShopKitApp() {
    }

    public static ShopKitApp getInstance() {
        if (instance == null) {
            instance = new ShopKitApp();
        }
        return instance;
    }

    public void setContext(Context ctx) {
        this.mContext = ctx;
    }

    public Context getAppContext() {
        return mContext;
    }

    public Activity getAppActivity() {
        return mActivity;
    }

    public void dismissMDNSelectorScreen() {
        mFragmentActivity.getSupportFragmentManager().popBackStack();
    }
}