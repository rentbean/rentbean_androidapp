package com.food.core.session;

import android.content.Context;
import android.util.Log;

import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by bonamsa on 2/13/16.
 */
public class PDHSessionManager {

    private String sessionpath = "logininfo";
    private static PDHSessionManager sharedInstance = null;

    private PDHSession session;

    private PDHSessionManager() {

    }

    public static PDHSessionManager getSessionManager() {
        if(sharedInstance == null) {
            sharedInstance = new PDHSessionManager();
        }

        return sharedInstance;
    }

    public PDHSession getSession() {

        if(session == null) {
            try {
                FileInputStream fis = PDHAppState.getInstance().applicationContext.openFileInput(sessionpath);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Object session = ois.readObject();

                if(session != null)
                    this.session = (PDHSession) session;

            } catch (IOException e) {
                Log.e(PDHConstants.AppTag,"IOException reading ");
                e.printStackTrace(System.err);
            } catch(ClassNotFoundException e) {
                e.printStackTrace(System.err);
            }
        } else {
            session = new PDHSession();
            saveSession();
        }

        return session;
    }

    public void saveSession() {

        if(session == null) {
            session = new PDHSession();
        }

        try {
            FileOutputStream fos = PDHAppState.getInstance().applicationContext.openFileOutput(sessionpath, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(session);
            os.close();
            fos.close();
        } catch (IOException e) {
            Log.e(PDHConstants.AppTag,"IOException saving ");
            e.printStackTrace(System.err);
        }
    }
}
