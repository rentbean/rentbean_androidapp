/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.common.http;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by bonamsa on 12/30/15.
 */
public class PDHAuthHeadersInterceptor implements Interceptor {

    private boolean httpSecured = false;

    public PDHAuthHeadersInterceptor(boolean httpSecured) {
        this.httpSecured = httpSecured;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request.Builder builder = chain.request().newBuilder();

     /* builder.addHeader("un","postpaid");
        builder.addHeader("pd","RYkI7oBDzpkm6XB0o5nJUQ==");
        builder.addHeader("cliendId","APP_ANDROID");
        builder.addHeader("user-agent","App-Android");*/

        builder.addHeader("Accept", "application/json");
        builder.addHeader("Host", "bemisaal.in");

        if(this.httpSecured) {
            //builder.addHeader("HTTPSecured","YES");
        } else {

        }

        return chain.proceed(builder.build());
    }
}
