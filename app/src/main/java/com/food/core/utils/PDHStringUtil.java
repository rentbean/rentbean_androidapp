/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

import android.util.Log;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by mishrki on 12/28/2015.
 */
public class PDHStringUtil {

    public static boolean isNull(String str) {
        if( str==null || "".equals(str)) {
            return true;
        } else {
            return false;
        }
    }

    public static String formatMTN(String originalMTN) {
        if(originalMTN!=null) {
            originalMTN = originalMTN.replaceAll("[^\\d]", "");
        }
        return formatToPhoneNumber(originalMTN);
    }

    public static String formatImageURL(String originalURL) {
        String url = originalURL;
        if(originalURL.startsWith("//")) {
            url = "http:" + PDHURLUtils.URLDecode(originalURL);
        }
        Log.i("StringUtil", url);
        return url;
    }

    public static String formatToTwoDecimal(String decimalValue) {
        Boolean prefixDollar = false;
        if(decimalValue.startsWith("$")) {
            prefixDollar = true;
            decimalValue = decimalValue.substring(1, decimalValue.length());
        }

        if(decimalValue.equals(".00")) {
            decimalValue = "0"+decimalValue;
            System.out.println(decimalValue);
        } else  if(decimalValue!= null && !decimalValue.isEmpty()){
            decimalValue = String.format("%.2f",Float.parseFloat(decimalValue));
        }
        Log.d(PDHConstants.AppTag, "Decimal Value is " + decimalValue);
        return prefixDollar ? "$"+decimalValue : decimalValue;
    }

    public static String setBGCFilterToS7(String url, String bgcFilter){

        Log.d(PDHConstants.AppTag, "Format BGC Initial URL "+url);
        String[] params = url.split("&");

        for (int i = 0; i < params.length; i++) {
            if(params[i].startsWith("bgc")) {
                url = url.replace(params[i],bgcFilter);
            }
        }

        if(!url.contains("?")) {
            url = url + "?"+bgcFilter;
        }

        Log.d(PDHConstants.AppTag, "Format BGC Final URL "+url);
        return url;
    }

    public static String formatToPhoneNumber(String number) {
        String output;
        if (number == null) return null;
        switch (number.length()) {
            case 7:
                output = String.format("%s-%s", number.substring(0,3), number.substring(3,7));
                break;
            case 10:
                output = String.format("%s-%s-%s", number.substring(0,3), number.substring(3,6), number.substring(6,10));
                break;
            case 11:
                output = String.format("%s (%s) %s-%s", number.substring(0,1) ,number.substring(1,4), number.substring(4,7), number.substring(7,11));
                break;
            case 12:
                output = String.format("+%s (%s) %s-%s", number.substring(0,2) ,number.substring(2,5), number.substring(5,8), number.substring(8,12));
                break;
            default:
                return number;
        }
        return output;
    }

    public static String createServerErrorString(String statusCode, String statusMessage, Map errorMap) {
        String errorMsg = "";
        if(!PDHStringUtil.isNull(statusMessage)) {
            errorMsg = statusMessage;
        }
       // errorMsg = errorMsg + " " + getErrorMessage(errorMap);
        if(!PDHStringUtil.isNull(statusCode)) {
            errorMsg = errorMsg + " Error Code ("+statusCode+")";
        }
        return errorMsg;
    }

    private static String getErrorMessage(Map errorMap) {
        if(errorMap == null || errorMap.size()==0) {
            return "Unknown Error. ";
        } else {
            StringBuffer errorMessage = new StringBuffer();
            Set keys = errorMap.keySet();
            Iterator iterator = keys.iterator();
            while(iterator.hasNext()) {
                Object key = iterator.next();
                if(errorMap.get(key)!=null) {
                    errorMessage.append(errorMap.get(key).toString());
                }
            }
            if(isNull(errorMessage.toString())) {
                return "Unknown Error. ";
            } else {
                return errorMessage.toString();
            }
        }
    }



}
