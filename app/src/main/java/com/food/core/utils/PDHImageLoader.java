/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

import android.content.Context;

import org.droidparts.net.image.ImageFetcher;

/**
 * Created by bonamsa on 12/7/15.
 */
public final class PDHImageLoader {

    private ImageFetcher imageFetcher;
    private static PDHImageLoader imageLoader;
    private PDHImageLoader(Context context) {
        super();
        imageFetcher = new ImageFetcher(context);
    }

    public static ImageFetcher getImageFetcher(Context context) {
        if(imageLoader == null) {
            imageLoader = new PDHImageLoader(context);
        }
        return imageLoader.imageFetcher;
    }
}
