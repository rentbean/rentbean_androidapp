/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by shaimu8 on 12/23/15.
 */
public class PDHURLUtils {
    public static String JSONData(Object obj, Type srcType, Boolean isToEncode) {
        Gson gson1 = new Gson();
        String jsonData = gson1.toJson(obj, srcType);
        String jsonString = jsonData;

        if (isToEncode) {
            try {
                jsonString = URLEncoder.encode(jsonData, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return jsonString;
    }

    public static String URLEncode(String value) {
        String encodedValue = null;
        if (value == null) return null;
        try {
            encodedValue = URLEncoder.encode(value,"utf-8");
        }catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return encodedValue;
    }

    public static String URLDecode(String value) {
        String decodedValue = null;
        if (value == null) return null;
        try {
            decodedValue = URLDecoder.decode(value, "utf-8");
        }catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return decodedValue;
    }
}
