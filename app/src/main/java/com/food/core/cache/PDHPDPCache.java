/*
 *   -------------------------------------------------------------------------
 *      PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *      Wireless, Inc. and its affiliates except under written
 *      agreement.
 *
 *      This is an unpublished, proprietary work of Verizon Wireless,
 *      Inc. that is protected by United States copyright laws.  Disclosure,
 *      copying, reproduction, merger, translation,modification,enhancement,
 *      or use by anyone other than authorized employees or licensees of
 *      Verizon Wireless, Inc. without the prior written consent of
 *      Verizon Wireless, Inc. is prohibited.
 *
 *      Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *   -------------------------------------------------------------------------
 */

package com.food.core.cache;

import android.content.ContentValues;
import android.content.Context;

import java.util.HashMap;
import java.util.List;

/**
 * Created by bonamsa on 2/7/16.
 */

public class PDHPDPCache {

    public static String pdpcache = "pdpcache";

    private static PDHCacheDB dbHelper = null;

    private static PDHPDPCache sharedInstance;

    private PDHPDPCache(Context context) {

        if(dbHelper == null) {
            dbHelper = PDHCacheDB.getInstance(context);
            dbHelper.onCreate(dbHelper.getWritableDatabase());
        }
    }

    public static PDHPDPCache getInstance(Context context) {

        if(sharedInstance == null) {
            sharedInstance = new PDHPDPCache(context);
        }

        return sharedInstance;
    }

    public HashMap<String, String> getDeviceDetails(String deviceProdId, String deviceSkuId, String selectedMtn) {

        List<HashMap<String, String>> resultSet = dbHelper.executeSelect("select * from "+pdpcache+" where deviceprodid='"+deviceProdId+"' and deviceskuid='"+deviceSkuId+"' and selectedmtn='"+selectedMtn+"'", null);

        if(resultSet == null || resultSet.size() == 0) {
            return null;
        }

        HashMap<String,String> row = resultSet.get(0);

        long time = Long.parseLong(row.get("lastrequested"));

        long currentTime = System.currentTimeMillis();

        if((currentTime-time) > PDHCacheDB.CACHE_TTL_MILLISECS) {
            deleteDeviceDetails(deviceProdId, deviceSkuId, selectedMtn);
            return null;
        }
        return row;
    }

    private int deleteDeviceDetails(String deviceProdId, String deviceSkuId, String selectedMtn) {

        dbHelper.executeDelete("delete from pdpcache where deviceprodid = '"+deviceProdId+"' and deviceskuid ='"+deviceSkuId+"' and selectedmtn = '"+selectedMtn+"'");

        return 1;
    }

    public boolean cacheDeviceDetails(String deviceProdId, String deviceSkuId, String selectedMtn, String jsonResponse) {

        ContentValues values = new ContentValues();
        values.put("deviceprodid",deviceProdId);
        values.put("deviceskuid",deviceSkuId);
        values.put("selectedmtn",selectedMtn);
        values.put("jsonresponse", jsonResponse);
        values.put("lastrequested", System.currentTimeMillis());

        return dbHelper.insertRow(pdpcache, values);
    }
}
