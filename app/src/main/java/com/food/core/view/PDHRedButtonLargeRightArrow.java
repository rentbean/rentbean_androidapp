/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.food.R;


public class PDHRedButtonLargeRightArrow extends RelativeLayout {
    private com.food.core.view.PDHRedButtonLarge button;
    private PDHIcon icon;
    private int textColor = 0;
    private int textColorDisabled = 0;

    public PDHRedButtonLargeRightArrow(Context context) { this(context, null); }
    public PDHRedButtonLargeRightArrow(Context context, AttributeSet attrs) { this(context, attrs, 0); }
    public PDHRedButtonLargeRightArrow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        float density = getResources().getDisplayMetrics().density;

        button = new PDHRedButtonLarge(context, attrs, defStyleAttr);
        button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        int[] tArrAttrs = new int[]{ android.R.attr.textColor };
        TypedArray tArr = context.obtainStyledAttributes(attrs, tArrAttrs);
        textColor = tArr.getColor(0, ContextCompat.getColor(context, R.color.red_button_text));
        textColorDisabled = ContextCompat.getColor(context, R.color.red_button_text_disabled);
        tArr.recycle();

        icon = new PDHIcon(context);
        icon.setText("" + ((char) 0xE609));
        icon.setTextSize(14f);
        icon.setTextColor(ContextCompat.getColor(context, R.color.white));
        LayoutParams ilp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        ilp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        ilp.addRule(RelativeLayout.CENTER_VERTICAL);
        ilp.rightMargin = (int) (density * PDHRedButtonLarge.DEFAULT_PADDING_SIZE);
        icon.setLayoutParams(ilp);
        icon.setEnabled(false);
        icon.setTextColor(textColor);


        this.addView(button);
        this.addView(icon);
    }

    public void setText(String pText) {
        if(button!=null && pText!=null) {
            button.setText(pText);
        }
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        button.setOnClickListener(l);
    }
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        button.setEnabled(enabled);
        icon.setTextColor(button.isEnabled() ? textColor : textColorDisabled);
    }
}
