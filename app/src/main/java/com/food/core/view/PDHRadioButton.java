/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.RadioButton;

public class PDHRadioButton extends RadioButton {
    private static final String TAG = "VZWRadioButton";
    private VzwRadioButtonIcon icon;

    public PDHRadioButton(Context context) { this(context, null); }
    public PDHRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }
    public PDHRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        setEnabled(true);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(null);
        }

        float density = this.getResources().getDisplayMetrics().density;
        icon = new VzwRadioButtonIcon(density);
        setButtonDrawable(icon);
        icon.setActiveInstant(isChecked());
        setMinHeight(icon.getSize());

        int minPadMar = (int)(density*10f);
        int padLeft = this.getPaddingLeft();
        int padTop = this.getPaddingTop();
        int padRight = this.getPaddingRight();
        int padBottom = this.getPaddingBottom();
        int minPadLeft = icon.getSize() + minPadMar;
        if(padLeft<minPadLeft) {
            padLeft = minPadLeft;
        }
        setPadding(padLeft, padTop, padRight, padBottom);
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        if(icon!=null) {
            icon.setActive(isChecked());
        }
        invalidate();
    }

    private class VzwRadioButtonIcon extends Drawable {
        private float dpSize = 20;
        private int pxSize;
        private float radius = 0;
        private Paint paintColor;
        private Paint paintClear;
        private int alpha = 255;
        private float density;
        private float lineWidthInactive = 2f;
        private float lineWidthActive = 6f;
        private float lineWidthRender = lineWidthInactive;
        private int colorInactive = 0xFF888888;
        private int colorActive = 0xFFCD040B;
        private int colorRender = colorInactive;
        private int colorTransparent = 0x00000000;
        private boolean active = false;
        ValueAnimator timeAnimator = ValueAnimator.ofFloat(0f, 1f);
        ArgbEvaluator rgbEval = new ArgbEvaluator();
        private long animationDuration = 250;
        private OvershootInterpolator animActiveInterpolator = new OvershootInterpolator();
        private DecelerateInterpolator animInactiveInterpolator = new DecelerateInterpolator();

        public VzwRadioButtonIcon(float pDensity) {
            density = pDensity;

            radius = dpSize*density*0.5f;
            pxSize = (int)(dpSize*density);
            lineWidthInactive *= density;
            lineWidthActive *= density;

            paintColor = new Paint();
            paintColor.setStyle(Paint.Style.FILL);
            paintColor.setColor(colorInactive);
            paintColor.setFlags(Paint.ANTI_ALIAS_FLAG);

            paintClear = new Paint();
            paintClear.setStyle(Paint.Style.FILL);
            paintClear.setColor(colorTransparent);
            paintClear.setFlags(Paint.ANTI_ALIAS_FLAG);
            paintClear.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

            setBounds(pxSize, pxSize, pxSize, pxSize);

            timeAnimator.setDuration(animationDuration);
            timeAnimator.setInterpolator(new LinearInterpolator());
            timeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    float progress = (float) animator.getAnimatedValue();
                    if (active) {
                        colorRender = (int) rgbEval.evaluate(progress, colorInactive, colorActive);
                        lineWidthRender = animActiveInterpolator.getInterpolation(progress)*(lineWidthActive - lineWidthInactive) + lineWidthInactive;
                    } else {
                        colorRender = (int) rgbEval.evaluate(progress, colorActive, colorInactive);
                        lineWidthRender = lineWidthActive - animInactiveInterpolator.getInterpolation(progress)*(lineWidthActive - lineWidthInactive);
                    }

                    invalidate();
                }
            });
        }

        public int getSize() {
            return pxSize;
        }

        public void setActive(boolean pActive) {
            if(pActive!=active) {
                active = pActive;
                animate();
            }
        }
        public void setActiveInstant(boolean pActive) {
            active = pActive;
            if(active) {
                colorRender = colorActive;
                lineWidthRender = lineWidthActive;
            } else {
                colorRender = colorInactive;
                lineWidthRender = lineWidthInactive;
            }
        }

        @Override
        public void draw(Canvas canvas) {
            paintColor.setColor(colorRender);
            canvas.drawRect(0, 0, pxSize, pxSize, paintClear);
            canvas.drawCircle(radius, radius, radius, paintColor);
            canvas.drawCircle(radius, radius, radius-lineWidthRender, paintClear);
        }

        @Override
        public void setAlpha(int pAlpha) {
            alpha = pAlpha;
        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {

        }

        @Override
        public int getOpacity() {
            return 0;
        }

        private void animate() {
            timeAnimator.cancel();
            //timeAnimator.setInterpolator(active ? animActiveInterpolator : animInactiveInterpolator);
            timeAnimator.start();
        }

        public void test() {

        }
    }
}
