/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.CheckBox;

import com.food.R;
import com.food.core.utils.PDHFontUtils;

public class PDHCheckBox extends CheckBox {
    private static final String TAG = "VzwCheckBox";
    private static final float DEFAULT_FONTSIZE = 14f;
    private static Bitmap CHECKICON_BITMAP = null;
    private VzwCheckBoxIcon icon;

    public PDHCheckBox(Context context) { this(context, null); }
    public PDHCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }
    public PDHCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        setEnabled(true);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(null);
        }

        int[] tArrAttrs = new int[]{ android.R.attr.textSize, android.R.attr.textColor };
        TypedArray tArr = context.obtainStyledAttributes(attrs, tArrAttrs);

        setTypeface(PDHFontUtils.getInstance().getFont(PDHFontUtils.FONT_NHG_TEXT));
        int size = tArr.getDimensionPixelSize(0, -1);
        if(size==-1) {
            setTextSize(TypedValue.COMPLEX_UNIT_SP, DEFAULT_FONTSIZE);
        } else {
            setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
        setTextColor(tArr.getColor(1, ContextCompat.getColor(context, R.color.textBody)));

        tArr.recycle();

        float density = this.getResources().getDisplayMetrics().density;
        icon = new VzwCheckBoxIcon(density);
        setButtonDrawable(icon);
        icon.setActiveInstant(isChecked());
        setMinHeight(icon.getSize());

        int minPadMar = (int)(density*10f);
        int padLeft = this.getPaddingLeft();
        int padTop = this.getPaddingTop();
        int padRight = this.getPaddingRight();
        int padBottom = this.getPaddingBottom();
        int minPadLeft = icon.getSize() + minPadMar;
        if(padLeft<minPadLeft) {
            padLeft = minPadLeft;
        }
        setPadding(padLeft, padTop, padRight, padBottom);
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        if(icon!=null) {
            icon.setActive(isChecked());
        }
        invalidate();
    }
    private static synchronized Bitmap getCheckIconBitmap(int pxSize) {
        if(CHECKICON_BITMAP==null) {
            String iconStr = Character.toString((char)0xE0A6);
            Paint paint = new Paint();
            paint.setTextSize(pxSize);
            paint.setTypeface( PDHFontUtils.getInstance().getFont(PDHFontUtils.FONT_VZWICON) );
            paint.setColor(0xFFFFFFFF);
            paint.setTextAlign(Paint.Align.LEFT);
            float baseline = -paint.ascent();
            int width = (int)(paint.measureText(iconStr)+0.5f);
            int height = (int)(baseline+paint.descent()+0.5f);
            int xOff = 0;
            int yOff = 0;
            if(width<height) {
                xOff = (int)((height-width)*0.5f);
                width = height;
            } else {
                yOff = (int)((width-height)*0.5f);
                height = width;
            }
            CHECKICON_BITMAP = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(CHECKICON_BITMAP);
            canvas.drawText(iconStr, xOff, yOff+baseline, paint);

            return CHECKICON_BITMAP;
        } else {
            return CHECKICON_BITMAP;
        }
    }
    private class VzwCheckBoxIcon extends Drawable {
        private float dpSize = 20;
        private int pxSize;
        private float radius = 0;
        private Paint paintColor;
        private Paint paintClear;
        private float density;
        private float lineWidthInactive = 2f;
        private float lineWidthActive = dpSize*0.5f;
        private float lineWidthRender = lineWidthInactive;
        private int colorInactive = 0xFF888888;
        private int colorActive = 0xFFCD040B;
        private int colorRender = colorInactive;
        private int colorTransparent = 0x00000000;
        private boolean active = false;
        ValueAnimator timeAnimator = ValueAnimator.ofFloat(0f, 1f);
        ArgbEvaluator rgbEval = new ArgbEvaluator();
        private long animationDuration = 250;
        private OvershootInterpolator overshootInterpolator = new OvershootInterpolator();
        private DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
        private Bitmap checkiconBitmap = null;
        private Rect checkiconSrc = new Rect();
        private Rect checkiconDst = new Rect();
        private float checkSizeInactive = 0f;
        private float checkSizeActive = 0.5f;
        private float checkSizeRender = checkSizeInactive;

        public VzwCheckBoxIcon(float pDensity) {
            density = pDensity;

            radius = dpSize*density*0.5f;
            pxSize = (int)(dpSize*density);
            lineWidthInactive *= density;
            lineWidthActive *= density;

            paintColor = new Paint();
            paintColor.setStyle(Paint.Style.FILL);
            paintColor.setColor(colorInactive);
            paintColor.setFlags(Paint.ANTI_ALIAS_FLAG);

            paintClear = new Paint();
            paintClear.setStyle(Paint.Style.FILL);
            paintClear.setColor(colorTransparent);
            paintClear.setFlags(Paint.ANTI_ALIAS_FLAG);
            paintClear.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

            checkiconBitmap = PDHCheckBox.getCheckIconBitmap(pxSize);
            checkiconSrc.set(0, 0, checkiconBitmap.getWidth(), checkiconBitmap.getHeight());
            Log.d(TAG, "Check Image Size: "+checkiconBitmap.getWidth()+" : "+checkiconBitmap.getHeight());

            setBounds(pxSize, pxSize, pxSize, pxSize);

            timeAnimator.setDuration(animationDuration);
            timeAnimator.setInterpolator(new LinearInterpolator());
            timeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    float progress = (float) animator.getAnimatedValue();
                    if (active) {
                        colorRender = (int) rgbEval.evaluate(progress, colorInactive, colorActive);
                        float lwp = progress*2f;
                        float csp = (progress*2f-0.8f)/1.2f;
                        lineWidthRender = decelerateInterpolator.getInterpolation(lwp>1 ? 1f : lwp)*(lineWidthActive - lineWidthInactive) + lineWidthInactive;
                        checkSizeRender = checkSizeActive*overshootInterpolator.getInterpolation(csp<0 ? 0 : csp);
                    } else {
                        colorRender = (int) rgbEval.evaluate(progress, colorActive, colorInactive);
                        lineWidthRender = lineWidthActive - decelerateInterpolator.getInterpolation(progress)*(lineWidthActive - lineWidthInactive);
                        checkSizeRender = checkSizeActive*(1f - decelerateInterpolator.getInterpolation(progress));
                    }

                    invalidate();
                }
            });
        }

        public int getSize() {
            return pxSize;
        }

        public void setActive(boolean pActive) {
            if(pActive!=active) {
                active = pActive;
                animate();
            }
        }
        public void setActiveInstant(boolean pActive) {
            active = pActive;
            if(active) {
                colorRender = colorActive;
                lineWidthRender = lineWidthActive;
                checkSizeRender = checkSizeActive;
            } else {
                colorRender = colorInactive;
                lineWidthRender = lineWidthInactive;
                checkSizeRender = checkSizeInactive;
            }
        }

        @Override
        public void draw(Canvas canvas) {
            paintColor.setColor(colorRender);
            canvas.drawRect(0, 0, pxSize, pxSize, paintColor);
            canvas.drawRect(lineWidthRender, lineWidthRender, pxSize - lineWidthRender, pxSize - lineWidthRender, paintClear);
            if(checkSizeRender>0) {
                int cw = (int)(checkSizeRender*pxSize);
                int co = (int)((pxSize-cw)*0.5f);
                checkiconDst.set(co, co, co+cw, co+cw);

                canvas.drawBitmap(checkiconBitmap, checkiconSrc, checkiconDst, null);
            }
        }

        @Override
        public void setAlpha(int pAlpha) {

        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {

        }

        @Override
        public int getOpacity() {
            return 0;
        }

        private void animate() {
            timeAnimator.cancel();
            timeAnimator.start();
        }

        public void test() {

        }
    }
}
