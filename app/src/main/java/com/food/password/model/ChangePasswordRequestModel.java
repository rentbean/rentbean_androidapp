package com.food.password.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class ChangePasswordRequestModel {
    private String username;
    private String password;
    private String newPassword;

    public ChangePasswordRequestModel(String username, String password, String newPassword) {
        this.username = username;
        this.password = password;
        this.newPassword = newPassword;
    }

}
