package com.food.rbnPDP.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alok on 10/10/16.
 */
public class RBNProductAvailabilityResponseModel extends PDHAPIDataModelBase {

    @SerializedName("output")
    @Expose
    public Output output;

    @SerializedName("errorMap")
    @Expose
    public ErrorMap errorMap;

    @SerializedName("serviceCode")
    @Expose
    public String serviceCode;

    @SerializedName("serviceMessage")
    @Expose
    public String serviceMessage;



    public class Output {

        @SerializedName("object")
        @Expose
        public Object object;

        public class Object{

            @SerializedName("skuAvailabilityMap")
            @Expose
            public HashMap<String, HashMap<String, Boolean>> skuAvailabilityMap;


        }

    }

    public class ErrorMap {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
