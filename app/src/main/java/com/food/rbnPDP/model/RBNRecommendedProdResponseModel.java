package com.food.rbnPDP.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by alok on 10/10/16.
 */
public class RBNRecommendedProdResponseModel extends PDHAPIDataModelBase {


    @SerializedName("output")
    @Expose
    public Output output;

    @SerializedName("errorMap")
    @Expose
    public ErrorMap errorMap;

    @SerializedName("serviceCode")
    @Expose
    public String serviceCode;

    @SerializedName("serviceMessage")
    @Expose
    public String serviceMessage;



    public class Output {

        @SerializedName("object")
        @Expose
        public Object object;

        public class Object{

            @SerializedName("productSummary")
            @Expose
            public ArrayList<ProductSummary> productSummary;

            public class ProductSummary {
                @SerializedName("productName")
                @Expose
                public String productName;

                @SerializedName("seoName")
                @Expose
                public String medImage;

                @SerializedName("medImage")
                @Expose
                public String name;

                @SerializedName("dailyPrice")
                @Expose
                public double dailyPrice;

                @SerializedName("weeklyprice")
                @Expose
                public double weeklyprice;

            }
        }

    }

    public class ErrorMap {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }


}
