package com.food.rbnPDP.service;

import com.food.rbnPDP.model.RBNProductAvailabilityResponseModel;
import com.food.rbnPDP.model.RBNRecommendedProdResponseModel;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by alok on 10/10/16.
 */
public interface RBNProductAvailabilityService {
   //http://rentbean.com/CatalogActor/getProductAvailability/?jsonData={"seoName":"BMW-Bike-K1300S"}

    @GET("/CatalogActor/getProductAvailability/")
    Call<RBNProductAvailabilityResponseModel> getProductAvailability(@Query("jsonData") String jsonData);
}
