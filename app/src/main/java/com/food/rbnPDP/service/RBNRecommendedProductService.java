package com.food.rbnPDP.service;

import com.food.rbnGridview.model.RBNGridwallListResponseModel;
import com.food.rbnPDP.model.RBNPDPListResponseModel;
import com.food.rbnPDP.model.RBNRecommendedProdResponseModel;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by alok on 10/10/16.
 */
public interface RBNRecommendedProductService {
   // http://rentbean.com/CatalogActor/getProductRecommendation/?jsonData={"seoName":"BMW-Bike-K1300S"}

    @GET("/CatalogActor/getProductRecommendation/")
    Call<RBNRecommendedProdResponseModel> getRecommendedProd(@Query("jsonData") String jsonData);
}
