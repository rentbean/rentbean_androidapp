package com.food.login.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class LoginRequestModel {
    private String username;
    private String password;

    public LoginRequestModel(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
