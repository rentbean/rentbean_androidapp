/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.rbnGridview.service;

import com.food.rbnGridview.model.RBNGridwallListResponseModel;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by shaimu8 on 12/15/15.
 */
public interface RBNGridwallListAPIService {

    @GET("/GridwallActor/getGridwallContent/?jsonData=%7B%22queryString%22%3A%22consoles%22%2C%22facetField%22%3A%5B%22Category%3Aconsoles%22%5D%7D")

    Call<RBNGridwallListResponseModel> getGridwallList();
}
