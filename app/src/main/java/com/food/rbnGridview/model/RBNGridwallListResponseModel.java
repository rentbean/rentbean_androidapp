package com.food.rbnGridview.model;

import com.bignerdranch.expandablerecyclerview.model.ParentListItem;
import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by agu186 on 2/20/2016.
 */
public class RBNGridwallListResponseModel extends PDHAPIDataModelBase {

    @SerializedName("output")
    @Expose
    public Output output;

    @SerializedName("errorMap")
    @Expose
    public ErrorMap errorMap;

    @SerializedName("serviceCode")
    @Expose
    public String serviceCode;

    @SerializedName("serviceMessage")
    @Expose
    public String serviceMessage;



    public class Output {

        @SerializedName("object")
        @Expose
        public Object object;

        public class Object{

            @SerializedName("records")
            @Expose
            public ArrayList<Records> records;

            @SerializedName("facetFields")
            @Expose
            public ArrayList<FacetFields> facetFields;

            @SerializedName("sortOptions")
            @Expose

            public HashMap<String, String> sortOptions;

            //  public SortOptions sortOptions;

            public class Records {
                @SerializedName("id")
                @Expose
                public String id;

                @SerializedName("skuId")
                @Expose
                public String skuId;

                @SerializedName("skuDescription")
                @Expose
                public String skuDescription;


                @SerializedName("skuName")
                @Expose
                public String skuName;


                @SerializedName("weeklyPrice")
                @Expose
                public double weeklyPrice;

                @SerializedName("deposit")
                @Expose
                public double deposit;

                @SerializedName("dailyPrice")
                @Expose
                public double dailyPrice;

                @SerializedName("monthlyPrice")
                @Expose
                public double monthlyPrice;

                @SerializedName("quaterlyPrice")
                @Expose
                public double quaterlyPrice;


                @SerializedName("largeImage")
                @Expose
                public String largeImage;

                @SerializedName("medImage")
                @Expose
                public String medImage;

                @SerializedName("thumbnailImage")
                @Expose
                public String thumbnailImage;

                @SerializedName("smallImage")
                @Expose
                public String smallImage;

                @SerializedName("aggregateRating")
                @Expose
                public double aggregateRating;

                @SerializedName("productName")
                @Expose
                public String productName;

                @SerializedName("productDescription")
                @Expose
                public String productDescription;

                @SerializedName("numOfReviews")
                @Expose
                public int numOfReviews;

                @SerializedName("brand")
                @Expose
                public String brand;

                @SerializedName("productId")
                @Expose
                public String productId;

                @SerializedName("parentCategoryName")
                @Expose
                public String parentCategoryName;

                @SerializedName("grandCategoryName")
                @Expose
                public String grandCategoryName;

                @SerializedName("grandCategoryId")
                @Expose
                public String grandCategoryId;

                @SerializedName("grandIsRoot")
                @Expose
                public boolean grandIsRoot;

                @SerializedName("rootCategoryName")
                @Expose
                public String rootCategoryName;

                @SerializedName("rootCategoryId")
                @Expose
                public String rootCategoryId;

                @SerializedName("seoName")
                @Expose
                public String seoName;

                @SerializedName("minTenure")
                @Expose
                public String minTenure;

                @SerializedName("installationCharges")
                @Expose
                public double installationCharges;

                @SerializedName("shippingPrice")
                @Expose
                public int shippingPrice;

                @SerializedName("displayMinTenure")
                @Expose
                public String displayMinTenure;

                @SerializedName("feature")
                @Expose
                public Features feature;

                public class Features {
                    @SerializedName("color")
                    @Expose
                    public String color;

                }

            }

            public class FacetFields  implements ParentListItem{
                public boolean isExpandable;
                private List<java.lang.Object> mChildrenList;

                @SerializedName("name")
                @Expose
                public String name;

                @SerializedName("queryName")
                @Expose
                public String queryName;

                @SerializedName("facetAttributes")
                @Expose
                public ArrayList<FacetAttributes> facetAttributes;

               /* @Override
                public List<java.lang.Object> getChildObjectList() {
                    return mChildrenList;
                }

                @Override
                public void setChildObjectList(List<java.lang.Object> list) {
                    mChildrenList = list;
                }*/


                @Override
                public List<java.lang.Object> getChildItemList() {
                    return mChildrenList;
                }

                public void setChildItemList(List<java.lang.Object> list) {
                    mChildrenList = list;
                }

                @Override
                public boolean isInitiallyExpanded() {

                    return !isExpandable;
                }

                public class FacetAttributes {
                    public boolean isSelected;
                    @SerializedName("name")
                    @Expose
                    public String name;

                    @SerializedName("count")
                    @Expose
                    public String count;

                    @SerializedName("facetValueId")
                    @Expose
                    public String facetValueId;

                }

            }

            public class SortOptions {

                @SerializedName("sku.deposita")
                @Expose
                public String deposita;

                @SerializedName("sku.dailyPricea")
                @Expose
                public String dailyPricea;

                @SerializedName("sku.dailyPriced")
                @Expose
                public String dailyPriced;

                @SerializedName("sku.depositd")
                @Expose
                public String depositd;



            }

        }

    }

    public class ErrorMap {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }


}
