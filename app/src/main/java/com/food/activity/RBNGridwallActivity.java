package com.food.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.bignerdranch.expandablerecyclerview.model.ParentListItem;
import com.food.R;
import com.food.adapter.RBNGridAdapter;
import com.food.adapter.RBNGridFilterAdapter;
import com.food.adapter.RBNGridSortAdapter;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHViewUtils;
import com.food.rbnFilter.provider.RBNGridFilterProvider;
import com.food.rbnGridview.model.RBNGridwallListResponseModel;
import com.food.rbnGridview.provider.RBNGridwallListProvider;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alok on 07/09/16.
 */
public class RBNGridwallActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private Toolbar mToolbar;
    SharedPreferences preferences;

    private RBNGridAdapter adapter;
    RecyclerView recyclerView;
    static RBNGridwallListResponseModel gridListData;
    private GridLayoutManager lLayout;// Declaring DrawerLayout

    private FrameLayout _container;
    private Button btnSort, btnFilter;
    private RBNSortFragment sortFragment;
    private RBNFilteringFragment filteringFragment;
    static ArrayList<ParentListItem> parentObjects;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rbn_gridview);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("ORDER HISTORY");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        btnSort =(Button)findViewById(R.id.btnSort);
        btnFilter =(Button)findViewById(R.id.btnFilter);
        btnSort.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        gridListData = new RBNGridwallListResponseModel();
        lLayout = new GridLayoutManager(RBNGridwallActivity.this, 2);
        recyclerView = (RecyclerView) findViewById(R.id.rbnGridView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);
        adapter = new RBNGridAdapter(this, gridListData);
        recyclerView.setAdapter(adapter);

        _container =(FrameLayout)findViewById(R.id.containerSortFilter);
        // Setting the adapter to RecyclerView
        getOrderHistory();


    }

    public void getOrderHistory(){
        RBNGridwallListProvider provider = new RBNGridwallListProvider();
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void getFilterData(){
        JSONObject json;
        boolean hasSelectedItem=false;
    try {
        String message;
        json = new JSONObject();

        json.put("queryString", gridListData.output.object.records.get(0).parentCategoryName);

        JsonArray jsonarray = new JsonArray();
      //  JSONObject jsonObj = new JSONObject();

        jsonarray.add("Category:"+gridListData.output.object.records.get(0).parentCategoryName);

       // jsonObj.put("Category", gridListData.output.object.records.get(0).parentCategoryName);
       // message = json.toString();
        for(int i =0; i<parentObjects.size();i++){
            //String str = ((RBNGridwallListResponseModel.Output.Object.FacetFields)parentObjects.get(i)).queryName;
            String str = "";
            boolean flag = false;
            for (int j=0;j<parentObjects.get(i).getChildItemList().size();j++){
                if(((RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes)parentObjects.get(i).getChildItemList().get(j)).isSelected){
                    hasSelectedItem = true;
                    flag = true;
                    str = str + ((RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes)parentObjects.get(i).getChildItemList().get(j)).facetValueId +" ";
                }
                //jsonObj.put(((RBNGridwallListResponseModel.Output.Object.FacetFields)parentObjects.get(i)).queryName, str);

              //  jsonarray.add(((RBNGridwallListResponseModel.Output.Object.FacetFields)parentObjects.get(i)).queryName +":"+str);
            }
          if(flag) {
            jsonarray.add(((RBNGridwallListResponseModel.Output.Object.FacetFields) parentObjects.get(i)).queryName + ":" + str);
         }
        }
       // jsonarray.put(jsonObj);
        json.put("facetField",jsonarray);

        if(hasSelectedItem) {
            RBNGridFilterProvider provider = new RBNGridFilterProvider(json.toString());
            provider.setCallback(this);
            provider.appContext = this;
            PDHViewUtils.getInstance().showProgress(this);
            provider.getData(PDHAppState.getInstance().loadFromServer);
        }

       }catch (Exception e) {

     }
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch(view.getId()){

            case R.id.btnFilter:
                if(sortFragment.isVisible()){
                    transaction.remove(sortFragment);
                    transaction.commit();
                }else{
                    transaction.remove(filteringFragment);
                    transaction.replace(R.id.containerSortFilter,sortFragment);
                    transaction.commit();
                }
                break;
            case R.id.btnSort:
               // FragmentTransaction transaction1 = getSupportFragmentManager().beginTransaction();
                if(filteringFragment.isVisible()){
                    transaction.remove(filteringFragment);
                    transaction.commit();
                }else{
                    transaction.remove(sortFragment);
                    transaction.replace(R.id.containerSortFilter,filteringFragment);
                    transaction.commit();
                }

                break;
        }
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof RBNGridwallListResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((RBNGridwallListResponseModel) model);
            }
        }
    }


    public void updateModel(RBNGridwallListResponseModel model_data) {
        if(model_data.serviceCode.equals(PDHConstants.SUCCESS_CODE)){
            gridListData =  model_data;
            adapter = new RBNGridAdapter(this,gridListData);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(lLayout);
            adapter = new RBNGridAdapter(this, gridListData);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            generateCrimes();
            sortFragment =new RBNSortFragment();
            filteringFragment =new RBNFilteringFragment();


        }
    }

    public static class RBNSortFragment extends Fragment
    {
        RecyclerView recyclerView;
        RBNGridSortAdapter adapter;
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

          //  return inflater.inflate(R.layout.fragment_rbn_sorting, container, false);

            View view = inflater.inflate(R.layout.fragment_rbn_sorting, container, false);

            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new com.food.util.LinearLayoutManager(getActivity()));

            adapter = new RBNGridSortAdapter(getActivity(), parentObjects);
            adapter.onRestoreInstanceState(savedInstanceState);

            recyclerView.setAdapter(adapter);



            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);


        }

        @Override public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            ((RBNGridSortAdapter) recyclerView.getAdapter()).onSaveInstanceState(outState);
        }

    }


    public static class RBNFilteringFragment extends Fragment
    {
        RecyclerView recyclerView;
        RBNGridFilterAdapter filterAdapter;
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            //  return inflater.inflate(R.layout.fragment_rbn_sorting, container, false);

            View view = inflater.inflate(R.layout.fragment_rbn_filter, container, false);

            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewFilter);
            recyclerView.setLayoutManager(new com.food.util.LinearLayoutManager(getActivity()));

            filterAdapter = new RBNGridFilterAdapter(getActivity(),gridListData.output.object.sortOptions);

            recyclerView.setAdapter(filterAdapter);




            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);


        }

    }


    public static ArrayList<ParentListItem> generateCrimes() {
        parentObjects = new ArrayList<>();
        for (RBNGridwallListResponseModel.Output.Object.FacetFields crime : gridListData.output.object.facetFields) {
           // ArrayList<RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes> childList = new ArrayList<>();
            //childList.add(crime.facetAttributes);
            crime.setChildItemList((List)crime.facetAttributes);
            parentObjects.add(crime);
        }
        return parentObjects;
    }


    /*private List<ParentListItem> generateCrimes() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();
        List<ParentListItem> parentListItems = new ArrayList<>();
        for (Crime crime : crimes) {
            List<CrimeChild> childItemList = new ArrayList<>();
            childItemList.add(new CrimeChild(crime.getDate(), crime.isSolved()));
            crime.setChildItemList(childItemList);
            parentListItems.add(crime);
        }
        return parentListItems;
    }*/

    public void setExpanded(int i){

        ((RBNGridwallListResponseModel.Output.Object.FacetFields)parentObjects.get(i)).isExpandable = false;

    }

    public void setCollapsed(int i){
        ((RBNGridwallListResponseModel.Output.Object.FacetFields)parentObjects.get(i)).isExpandable = true;
    }

    public void setCheckItem(int parentPos,int childPos,boolean flag){
        ((RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes)((RBNGridwallListResponseModel.Output.Object.FacetFields)parentObjects.get(parentPos)).getChildItemList().get(childPos)).isSelected = flag;
        getFilterData();
    }


}
