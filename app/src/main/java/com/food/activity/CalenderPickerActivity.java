package com.food.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.internal.BoltsMeasurementEventListener;
import com.food.R;
import com.food.util.AppPreferences;
import com.squareup.timessquare.CalendarPickerView;

import java.lang.reflect.Array;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by alok on 19/10/16.
 */
public class CalenderPickerActivity extends AppCompatActivity {
    private CalendarPickerView calendar;
    private static final String TAG = "CalenderPickerActivity";
    private Toolbar mToolbar;
    private HashMap<String, Boolean> mapProductAvailable;
    private ArrayList<Date> availableDates;
    private HashMap<Date,String> dateMap = new HashMap<Date, String>();

    private HashMap<Date,Boolean> datemap = new HashMap<Date, Boolean>();

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_square);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);//disable back button

       context = this;
       // Calendar nextYear = Calendar.getInstance();
        //nextYear.add(Calendar.YEAR, 1);

        calendar = (CalendarPickerView) findViewById(R.id.calendar_view);


        Date today = new Date();

       /* Calendar cal = Calendar.getInstance();

        cal.setTime(today);
        cal.add(Calendar.DAY_OF_YEAR, 1);*/


        mapProductAvailable =(HashMap<String, Boolean>) getIntent().getSerializableExtra("map");

        Set<String> availabilitySet = mapProductAvailable.keySet();

        availableDates = new ArrayList<Date>();



        for(int i=0;i<availabilitySet.toArray().length;i++){
            String key = (String)availabilitySet.toArray()[i];
            String day = key.split("-")[0];
            String month = key.split("#")[0].split("-")[1];
            String time = key.split("#")[1];
            Boolean flag =  mapProductAvailable.get(availabilitySet.toArray()[i]);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateInString = today.toString().split(" ")[today.toString().split(" ").length-1].toString()+"-"+month+"-"+day+" "+time+":"+00+":"+00;
            Date date = null;
            try {
                date = sdf.parse(dateInString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            datemap.put(date,flag);


        }

        Map<Date, Boolean> treeMap = new TreeMap<Date, Boolean>(datemap);

        boolean timeFlag = false;
        int cnt =0;
        String timeBool="";

        for(int i=0;i<treeMap.keySet().size();i++){
            Date date = (Date)treeMap.keySet().toArray()[i];
            Boolean flag = treeMap.get(date);

            if(flag){
                timeFlag = flag;
            }
           timeBool = timeBool +" "+ flag;
            cnt = cnt+1;
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
           if(cnt == 2){
              dateMap.put(date,timeBool);
               if(timeFlag) {
                   availableDates.add(date);
                   timeBool = "";
               }
               cnt = 0;
           }
        }

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);



        //.withHighlightedDate(cal.getTime())
        calendar.init(availableDates.get(0), nextYear.getTime())
                 .withSelectedDate(today)
                .inMode(CalendarPickerView.SelectionMode.SINGLE);
        calendar.highlightDates(availableDates);

        calendar.setOnInvalidDateSelectedListener(null);
        calendar.setDateSelectableFilter(new CalendarPickerView.DateSelectableFilter() {

            Calendar cal=Calendar.getInstance();
            @Override
            public boolean isDateSelectable(Date date) {
                boolean isSelecteable=true;
                cal.setTime(date);
               // int dayOfWeek=cal.get(Calendar.DAY_OF_WEEK);

                //disable if weekend
                //if(dayOfWeek==Calendar.SATURDAY || dayOfWeek==Calendar.SUNDAY){
                  //  isSelecteable=false;
                //}

             if(!availableDates.contains(date)){
                 isSelecteable =false;
             }
                if(isSelecteable){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                    LayoutInflater factory = LayoutInflater.from(context);
                    final View deleteDialogView = factory.inflate(
                            R.layout.dialog_select_choose_time_interval, null);
                    final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
                    deleteDialog.setView(deleteDialogView);


                    final TextView firstTimeSlot =(TextView)deleteDialogView.findViewById(R.id.time_slot_first);
                    final TextView secondTimeSlot =(TextView)deleteDialogView.findViewById(R.id.time_slot_second);


                    TextView addToOrderBtn =(TextView)deleteDialogView.findViewById(R.id.addToOrderBtn);

                    addToOrderBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                           // addToCart(AppPreferences.getUserEmail(prefs),record.Id, String.valueOf(fullPriceQuantityText), "1", record.FullPrice);
                            // deleteDialog.dismiss();
                        }
                    });
                    deleteDialog.show();
                }

                return isSelecteable;
            }
        });


    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings:
                return true;
            case R.id.action_next:
                ArrayList<Date> selectedDates = (ArrayList<Date>)calendar
                        .getSelectedDates();
                Toast.makeText(MainActivity.this, selectedDates.toString(),
                        Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Date> getHolidays(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "21-04-2015";
        Date date = null;
        try {
            date = sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<Date> holidays = new ArrayList<>();
        holidays.add(date);
        return holidays;
    }*/
}
