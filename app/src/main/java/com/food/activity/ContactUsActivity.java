package com.food.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.food.R;
import com.food.cart.model.AddItemToCartResponseModel;
import com.food.cart.model.EditItemToCartResponseModel;
import com.food.categoryMenuList.model.MenuListResponseModel;
import com.food.categoryMenuList.provider.MenuListProvider;
import com.food.contactUs.model.ContactUsResponseModel;
import com.food.contactUs.provider.ContactUsProvider;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 1/8/2016.
 */
public class ContactUsActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private Toolbar mToolbar;
    private TextView txtContactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        txtContactUs = (TextView) findViewById(R.id.txtContactUs);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("Contact Us");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        getContctUs();
    }


    public void getContctUs(){
        ContactUsProvider provider = new ContactUsProvider();
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void onclickHandler(View view) {

        switch (view.getId()) {
            case 1:
                break;
            default:
                break;
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof ContactUsResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((ContactUsResponseModel) model);
            }

        }
    }

    public void updateModel(ContactUsResponseModel model) {
        /*if(model.Result.ErrorCode.equals("0")){
        }*/
    }
}