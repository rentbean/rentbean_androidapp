package com.food.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.R;
import com.food.adapter.RBNGridAdapter;
import com.food.adapter.SlideImageAdapter;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHImageLoader;
import com.food.core.utils.PDHViewUtils;
import com.food.rbnGridview.model.RBNGridwallListResponseModel;
import com.food.rbnGridview.provider.RBNGridwallListProvider;
import com.food.rbnPDP.model.RBNPDPListResponseModel;
import com.food.rbnPDP.model.RBNProductAvailabilityResponseModel;
import com.food.rbnPDP.model.RBNRecommendedProdResponseModel;
import com.food.rbnPDP.provider.RBNPDPListProvider;
import com.food.rbnPDP.provider.RBNProductAvailabilityProvider;
import com.food.rbnPDP.provider.RBNRecommendedProdProvider;
import com.food.rbnPDP.service.RBNProductAvailabilityService;
import com.viewpagerindicator.CirclePageIndicator;


import org.droidparts.net.image.ImageFetchListener;
import org.droidparts.net.image.ImageFetcher;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by alok on 07/09/16.
 */
public class RBNPDPActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener, ImageFetchListener {

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    private EditText fromDateEtxt;
    private EditText toDateEtxt;

    TextView productName;
    ImageView productImage;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES= {R.mipmap.rent_default,R.mipmap.rent_default,R.mipmap.rent_default,R.mipmap.rent_default};
    private ArrayList<String> ImagesArray = new ArrayList<String>();
    private String SEO_NAME;
    private HashMap<String, Boolean> productAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rbn_pdp);
        productName = (TextView) findViewById(R.id.productName);
        productImage = (ImageView) findViewById(R.id.productImage);

        fromDateEtxt = (EditText) findViewById(R.id.startDate);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

        toDateEtxt = (EditText) findViewById(R.id.endDate);
        toDateEtxt.setInputType(InputType.TYPE_NULL);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SEO_NAME = getIntent().getStringExtra("SEO_NAME");
        setDateTimeField();
        //init();
        try {
            getPDPDetail();
        }catch(Exception e){

        }
    }

    private void init(ArrayList<RBNRecommendedProdResponseModel.Output.Object.ProductSummary> prodSummary) {
        for (int i = 0; i < prodSummary.size(); i++) {
            ImagesArray.add(prodSummary.get(i).name);
        }
        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlideImageAdapter(RBNPDPActivity.this, ImagesArray));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = prodSummary.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    public void getPDPDetail() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("seoName", SEO_NAME);

        RBNPDPListProvider provider = new RBNPDPListProvider(object.toString());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void getRecommendedProduct()  throws JSONException  {
        JSONObject object = new JSONObject();
        object.put("seoName", SEO_NAME);

        RBNRecommendedProdProvider provider = new RBNRecommendedProdProvider(object.toString());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void getProductAvailibility()  throws JSONException  {
        JSONObject object = new JSONObject();
        object.put("seoName", SEO_NAME);

        RBNProductAvailabilityProvider provider = new RBNProductAvailabilityProvider(object.toString());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }


    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);
        toDateEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startDate) {
            //fromDatePickerDialog.show();
           /* Intent i = new Intent(this, CalenderPickerActivity.class);
            startActivity(i);
            finish(); */


            Intent i = new Intent(RBNPDPActivity.this,CalenderPickerActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("map",productAvailable);
            startActivity(i);
            finish();

            /*CalenderPickerActivity newFragment = new CalenderPickerActivity();
            newFragment.show(getSupportFragmentManager(), "dialog");*/

        } else if (view.getId() == R.id.endDate) {
           // toDatePickerDialog.show();

            Intent i = new Intent(RBNPDPActivity.this,CalenderPickerActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("map",productAvailable);
            startActivity(i);
            finish();

        }
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof RBNPDPListResponseModel) {
            if (errorString == null) {
                //PDHViewUtils.getInstance().hideProgress();
                updateModel((RBNPDPListResponseModel) model);
            }
        } else if (model instanceof RBNRecommendedProdResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateRecommendedProductModel((RBNRecommendedProdResponseModel) model);
            }
        }
        else if (model instanceof RBNProductAvailabilityResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateProductAvaialibilityModel((RBNProductAvailabilityResponseModel) model);
            }
        }

    }


    public void updateModel(RBNPDPListResponseModel model_data) {
        if (model_data.serviceCode.equals(PDHConstants.SUCCESS_CODE)) {
            productName.setText(model_data.output.object.prodcuct.name);
            ImageFetcher imageFetcher = PDHImageLoader.getImageFetcher(this);
            productImage.setTag("MenuImage");
            imageFetcher.attachImage("http://cdn.rentbean.com/" + model_data.output.object.skus.get(0).skuImage.get(0).largeImage, productImage, null, 0, this);
            try {
                getRecommendedProduct();
                getProductAvailibility();

            } catch (Exception e) {

            }
        }
    }
    public void updateRecommendedProductModel(RBNRecommendedProdResponseModel model_data) {
        if (model_data.serviceCode.equals(PDHConstants.SUCCESS_CODE)) {
            init(model_data.output.object.productSummary);
        }
    }
    public void updateProductAvaialibilityModel(RBNProductAvailabilityResponseModel model_data) {
        if (model_data.serviceCode.equals(PDHConstants.SUCCESS_CODE)) {
            Set<String> keyset = model_data.output.object.skuAvailabilityMap.keySet();
            productAvailable = model_data.output.object.skuAvailabilityMap.get(keyset.toArray()[0]);
        }
    }

    @Override
    public void onFetchAdded(ImageView imageView, String imgUrl) {

    }

    @Override
    public void onFetchProgressChanged(ImageView imageView, String imgUrl, int kBTotal, int kBReceived) {

    }

    @Override
    public void onFetchFailed(ImageView imageView, String imgUrl, Exception e) {

    }

    @Override
    public void onFetchCompleted(ImageView imageView, String imgUrl, Bitmap bm) {

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();


    }
}
