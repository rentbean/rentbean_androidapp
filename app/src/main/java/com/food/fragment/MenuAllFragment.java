package com.food.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.food.R;
import com.food.adapter.PageOneAdapter;
import com.food.categoryMenuList.model.MenuListResponseModel;

import java.util.ArrayList;

public class MenuAllFragment extends Fragment
{
    private BaseAdapter adapter;
    ListView listView;
    int selPos;
    private ArrayList<MenuListResponseModel.Output.RecordList> itemsArray = new ArrayList<MenuListResponseModel.Output.RecordList>();

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

       itemsArray = (ArrayList<MenuListResponseModel.Output.RecordList>)getArguments().get("MENUDATA");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        return inflater.inflate(R.layout.menu_all_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        listView = (ListView) getActivity().findViewById(R.id.list_1);
        adapter = new PageOneAdapter(getActivity(),itemsArray);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

   public void setData(ArrayList<MenuListResponseModel.Output.RecordList> itemsArray){
       this.itemsArray = itemsArray;
       adapter = new PageOneAdapter(getActivity(),itemsArray);
       listView.setAdapter(adapter);
       adapter.notifyDataSetChanged();
    }

}
