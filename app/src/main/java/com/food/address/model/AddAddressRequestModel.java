package com.food.address.model;

import com.food.core.service.PDHAPIDataModelBase;

/**
 * Created by agu186 on 2/20/2016.
 */
public class AddAddressRequestModel extends PDHAPIDataModelBase {
    private String uid;
    private String name;
    private String address;
    private String locality;
    private String pincode;
    private String mobile;


    public AddAddressRequestModel(String uid, String name, String address, String locality, String pincode, String mobile) {
        this.uid = uid;
        this.name = name;
        this.address = address;
        this.locality = locality;
        this.pincode = pincode;
        this.mobile = mobile;
    }
}
