package com.food.categoryMenuList.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public  class MenuListResponseModel extends PDHAPIDataModelBase implements Serializable {

    @SerializedName("Result")
    @Expose
    public Output Result;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
    public class Output {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

        public ArrayList<RecordList> Record;

        public class RecordList {
            @SerializedName("DishOfWeek")
            @Expose
            public String DishOfWeek;
            @SerializedName("DishType")
            @Expose
            public String DishType;
            @SerializedName("FullPrice")
            @Expose
            public String FullPrice;
            @SerializedName("HalfPrice")
            @Expose
            public String HalfPrice;
            @SerializedName("Id")
            @Expose
            public String Id;
            @SerializedName("Image")
            @Expose
            public String Image;
            @SerializedName("Title")
            @Expose
            public String Title;

            public boolean isAddToCart;

            public String toString() {
                return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
            }
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }
    }
}
