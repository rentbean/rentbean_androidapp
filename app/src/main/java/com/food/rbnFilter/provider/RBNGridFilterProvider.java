package com.food.rbnFilter.provider;


import android.util.Log;

import com.food.R;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.rbnFilter.model.RBNGridFilterRequestModel;
import com.food.rbnFilter.service.RBNGridFilterAPIService;
import com.food.rbnGridview.model.RBNGridwallListResponseModel;
import com.food.registration.provider.RegistrationProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by agu186 on 2/20/2016.
 */
public class RBNGridFilterProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private String selectedMtn = null;
    private Boolean loanInfoRequired = null;
    private Boolean fullInfoRequired = null;

    public String jsonName = null;
    public String json;

    public RBNGridFilterProvider(String _json){
     json = _json;
    }

    @Override
    public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.base_host_url))
                .addConverterFactory(converterFactory)
                //.client(PDHHTTPClientFactory.getVZWHTTPClientReadCookiesFromHeader(CategoryListProvider.lastRequestSuccess, false))
                .build();
        RBNGridFilterAPIService service = retrofit.create(RBNGridFilterAPIService.class);
        //Call<RBNGridwallListResponseModel> call = service.getPDPList(json);
        Call<RBNGridwallListResponseModel> call = service.getPDPList();
        call.enqueue(new Callback<RBNGridwallListResponseModel>() {
            @Override
            public void onResponse(Response<RBNGridwallListResponseModel> response, Retrofit var) {
                RBNGridwallListResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SUCCESS_CODE.equals(model.serviceCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());
                    errorString = PDHStringUtil.createServerErrorString(model.errorMap.ErrorCode, model.errorMap.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                RegistrationProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            RBNGridFilterRequestModel model = gson.fromJson(bufferString, RBNGridFilterRequestModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
