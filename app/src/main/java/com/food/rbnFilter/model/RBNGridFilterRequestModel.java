package com.food.rbnFilter.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by agu186 on 2/20/2016.
 */
public class RBNGridFilterRequestModel extends PDHAPIDataModelBase {

    @SerializedName("queryString")
    @Expose
    public String queryString;

    @SerializedName("facetField")
    @Expose
    public HashMap<String, String> facetField;


}
