package com.food.placeOrder.service;

import com.food.placeOrder.model.PlaceOrderRequestModel;
import com.food.placeOrder.model.PlaceOrderResponseModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;


/**
 * Author by hemendra.kumar on 4/8/2016.
 */
public interface PlaceOrderAPIService {
    @Headers({
            "Accept: application/json",
            "Host: bemisaal.in"
    })

    @POST("/restAppNew/api/orders/add.json")
    Call<PlaceOrderResponseModel> getPlaceOrder(@Body PlaceOrderRequestModel placeOrderRequestModel);
}


