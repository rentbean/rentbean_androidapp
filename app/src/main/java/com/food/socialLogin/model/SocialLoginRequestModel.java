package com.food.socialLogin.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SocialLoginRequestModel {
    private String name;
    private String email;
    private String providerId;
    private String provider;

    public SocialLoginRequestModel(String name, String email,String providerId,String provider) {
        this.name = name;
        this.email = email;
        this.providerId = providerId;
        this.provider=provider;
    }
}
