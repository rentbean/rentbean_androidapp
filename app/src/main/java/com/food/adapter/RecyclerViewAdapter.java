package com.food.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.food.R;
import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.core.utils.PDHImageLoader;

import org.droidparts.net.image.ImageFetchListener;
import org.droidparts.net.image.ImageFetcher;

import java.util.List;

/**
 * Author by hemendra.kumar on 1/11/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> implements ImageFetchListener {
    private List<CategoryListResponseModel.Output.RecordList> itemList;
    private Context context;
    public RecyclerViewAdapter(Context context, List<CategoryListResponseModel.Output.RecordList> itemList) {
        this.itemList = itemList;
        this.context = context;
    }
    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_item, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }
    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.menuName.setText(itemList.get(position).Name);
   //   holder.menuImage.setImageBitmap(ApplicationUtil.roundCornerImage(BitmapFactory.decodeResource(context.getResources(), itemList.get(position).getPhoto()), 50));
        ImageFetcher imageFetcher = PDHImageLoader.getImageFetcher(context);
        holder.menuImage.setTag("MenuImage");
        imageFetcher.attachImage(itemList.get(position).Image,  holder.menuImage, null, 0, this);
    }
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    @Override
    public void onFetchAdded(ImageView imageView, String imgUrl) {
        if(imageView.getTag().equals("MenuImage")) {
            imageView.setImageResource(R.mipmap.log);
        }
    }

    @Override
    public void onFetchProgressChanged(ImageView imageView, String imgUrl, int kBTotal, int kBReceived) {

    }

    @Override
    public void onFetchFailed(ImageView imageView, String imgUrl, Exception e) {

    }

    @Override
    public void onFetchCompleted(ImageView imageView, String imgUrl, Bitmap bm) {

    }
}