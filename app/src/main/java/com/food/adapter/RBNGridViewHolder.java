package com.food.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.food.R;
import com.food.activity.HomeActivity;

/**
 * Created by alok on 07/09/16.
 */
public class RBNGridViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView menuName;
    public ImageView menuImage;
    private Context context;

    public RBNGridViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        itemView.setOnClickListener(this);
        menuName = (TextView)itemView.findViewById(R.id.menu_Image_TextView);
        menuImage = (ImageView)itemView.findViewById(R.id.menu_ImageView);

    }
    @Override
    public void onClick(View view) {
        if(context instanceof HomeActivity) {
            ((HomeActivity) context).moveMenuScreen(getPosition());
        }
        Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
