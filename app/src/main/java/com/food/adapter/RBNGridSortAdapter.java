package com.food.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;


import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bignerdranch.expandablerecyclerview.model.ParentListItem;
import com.food.R;
import com.food.activity.RBNGridwallActivity;
import com.food.rbnGridview.model.RBNGridwallListResponseModel;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by alok on 22/09/16.
 */
 public class RBNGridSortAdapter extends ExpandableRecyclerAdapter<RBNGridSortAdapter.RBNGridParentViewHolder,RBNGridSortAdapter.RBNGridChildViewHolder>  {
    private LayoutInflater mInflater;
    private Context context;
    private ArrayList<Integer> collapseParentItem;
    private List<ParentListItem> parentItemList;
    public RBNGridSortAdapter(Context context, List<ParentListItem> parentItemList) {
        super(parentItemList);
        this.context =context;
        this.parentItemList = parentItemList;
        collapseParentItem = new ArrayList<Integer>();
        for(int i=0;i<parentItemList.size();i++){
            if(((RBNGridwallListResponseModel.Output.Object.FacetFields)parentItemList.get(i)).isExpandable){
                collapseParentItem.add(i);
            }
        }
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public RBNGridParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_rbn_grid_parent, viewGroup, false);
       // RBNGridParentViewHolder parentViewHolder = new RBNGridParentViewHolder(view);

        return new RBNGridParentViewHolder(view);
    }

    @Override
    public RBNGridChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_rbn_grid_child, viewGroup, false);
        return new RBNGridChildViewHolder(view);
    }




    @Override
    public void onBindParentViewHolder(RBNGridParentViewHolder rbnGridParentViewHolder, final int i, ParentListItem o) {
        RBNGridwallListResponseModel.Output.Object.FacetFields crime = (RBNGridwallListResponseModel.Output.Object.FacetFields) o;

      /*  rbnGridParentViewHolder.setParentListItemExpandCollapseListener(new ParentViewHolder.ParentListItemExpandCollapseListener() {
            @Override
               public void onParentListItemExpanded(int i) {
                    System.out.println("expand: " + i);

                   // collapseAllParents();
                }
            @Override
               public void onParentListItemCollapsed(int i) {
                    System.out.println("collapsed:" + i);
                ((RBNGridwallActivity)context).setCollapsed(i);
                }
            });*/


////          VERSION2
        /*rbnGridParentViewHolder.mCrimeTitleTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                System.out.println("touch");
                collapseAllParents();
                return false;
            }
        });*/

        rbnGridParentViewHolder.mCrimeTitleTextView.setText(crime.name);

    }


    @Override
    public void onParentListItemCollapsed(int position) {
        int currentParentItem=0;
        int count=0;
        int k=0;
        boolean haveExpanded = false;


        if(!(count ==position)) {
            for (int i = 0; i < parentItemList.size(); i++) {
                if (!collapseParentItem.contains(i)) {
                    haveExpanded = true;
                    count = count + parentItemList.get(i).getChildItemList().size();
                    k = k+1;
                } else {
                    count = count + 1;
                }
                if(haveExpanded){
                    if (((count+k) == position)) {
                        currentParentItem = i + 1;
                        collapseParentItem.add(currentParentItem);
                        break;
                    }
                }else {
                    if ((count == position)) {
                        currentParentItem = i+1;
                        collapseParentItem.add(currentParentItem);
                        break;
                    }
                }

            }
        }else{
            collapseParentItem.add(currentParentItem);
        }

        ((RBNGridwallActivity)context).setCollapsed(currentParentItem);

        super.onParentListItemCollapsed(position);
    }

        @Override
        public void onParentListItemExpanded(int position) {
            int currentParentItem=0;
            int count=0;
            int k=0;
            boolean haveExpanded = false;


            if(!(count ==position)) {
                for (int i = 0; i < parentItemList.size(); i++) {
                    if (!collapseParentItem.contains(i)) {
                        haveExpanded = true;
                        count = count + parentItemList.get(i).getChildItemList().size();
                        k = k+1;
                    } else {
                        count = count + 1;
                    }
                    if(haveExpanded){
                        if ((count+k) == position) {
                            currentParentItem = i+1;
                            //collapseParentItem.add(currentParentItem);
                           // collapseParentItem.remove(currentParentItem-k);

                            for(int l = 0;l<collapseParentItem.size();l++){
                                if(collapseParentItem.get(l)==currentParentItem){
                                    collapseParentItem.remove(l);
                                }
                            }
                            break;
                        }
                    }else {
                        if ((count == position)) {
                            currentParentItem = i+1;
                            //collapseParentItem.add(currentParentItem);
                            //collapseParentItem.remove(i);
                            for(int l = 0;l<collapseParentItem.size();l++){
                                if(collapseParentItem.get(l)==currentParentItem){
                                    collapseParentItem.remove(l);
                                }
                            }
                            break;
                        }
                    }

                }
            }else{
                for(int l = 0;l<collapseParentItem.size();l++){
                    if(collapseParentItem.get(l)==currentParentItem){
                        collapseParentItem.remove(l);
                    }
                }
            }


            ((RBNGridwallActivity)context).setExpanded(currentParentItem);
         super.onParentListItemExpanded(position);
    }

    @Override
    public void onBindChildViewHolder(RBNGridChildViewHolder rbnGridChildViewHolder, final int parentPosition, final int childPosition, Object o) {
         RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes crimeChild = (RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes) o;
        rbnGridChildViewHolder.mCrimeSolvedCheckBox.setText(crimeChild.name+" "+crimeChild.count);

        rbnGridChildViewHolder.mCrimeSolvedCheckBox.setChecked(((RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes)parentItemList.get(parentPosition).getChildItemList().get(childPosition)).isSelected);

        rbnGridChildViewHolder.mCrimeSolvedCheckBox.setTag(((RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes)parentItemList.get(parentPosition).getChildItemList().get(childPosition)));


        rbnGridChildViewHolder.mCrimeSolvedCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes  contact = ( RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes) cb.getTag();

                contact.isSelected = cb.isChecked();
                ((RBNGridwallListResponseModel.Output.Object.FacetFields.FacetAttributes)parentItemList.get(parentPosition).getChildItemList().get(childPosition)).isSelected =cb.isChecked();

                Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: " + cb.getText() + " is "
                                + cb.isChecked(), Toast.LENGTH_LONG).show();

                ((RBNGridwallActivity)context).setCheckItem(parentPosition,childPosition,cb.isChecked());
            }
        });






       /* rbnGridChildViewHolder.mCrimeSolvedCheckBox.setChecked(crimeChild.isSelected);
        rbnGridChildViewHolder.mCrimeSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((RBNGridwallActivity)context).setCheckItem(parentPosition,childPosition,b);
            }
        });*/
    }

    public class RBNGridChildViewHolder extends ChildViewHolder {

        public CheckBox mCrimeSolvedCheckBox;

        public RBNGridChildViewHolder(View itemView) {
            super(itemView);
            mCrimeSolvedCheckBox = (CheckBox) itemView.findViewById(R.id.child_list_item_crime_solved_check_box);
        }
    }

    public class RBNGridParentViewHolder extends ParentViewHolder {

        public TextView mCrimeTitleTextView;
        public RBNGridParentViewHolder(View itemView) {
            super(itemView);
            mCrimeTitleTextView = (TextView) itemView.findViewById(R.id.parent_list_item_crime_title_text_view);

        }
    }
}
