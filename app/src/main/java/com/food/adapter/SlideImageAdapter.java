package com.food.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.food.R;
import com.food.core.utils.PDHImageLoader;

import org.droidparts.net.image.ImageFetchListener;
import org.droidparts.net.image.ImageFetcher;

import java.util.ArrayList;

/**
 * Created by alok on 11/09/16.
 */

public class SlideImageAdapter extends PagerAdapter implements ImageFetchListener {


    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public SlideImageAdapter(Context context, ArrayList<String> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slideimages_layout, view, false);

      //  assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image_view_pager);


        //imageView.setImageResource(IMAGES.get(position));

        ImageFetcher imageFetcher = PDHImageLoader.getImageFetcher(context);
        imageView.setTag("RecommendedImage");
        imageFetcher.attachImage("http://cdn.rentbean.com/" + IMAGES.get(position), imageView, null, 0, this);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public void onFetchAdded(ImageView imageView, String imgUrl) {

    }

    @Override
    public void onFetchProgressChanged(ImageView imageView, String imgUrl, int kBTotal, int kBReceived) {

    }

    @Override
    public void onFetchFailed(ImageView imageView, String imgUrl, Exception e) {

    }

    @Override
    public void onFetchCompleted(ImageView imageView, String imgUrl, Bitmap bm) {

    }
}