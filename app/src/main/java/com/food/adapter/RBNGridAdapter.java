package com.food.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.food.R;
import com.food.activity.RBNPDPActivity;
import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.core.utils.PDHImageLoader;
import com.food.rbnGridview.model.RBNGridwallListResponseModel;

import org.droidparts.net.image.ImageFetchListener;
import org.droidparts.net.image.ImageFetcher;

import java.util.List;

/**
 * Created by alok on 07/09/16.
 */
public class RBNGridAdapter extends RecyclerView.Adapter<RecyclerViewHolders> implements ImageFetchListener {
    private RBNGridwallListResponseModel itemList;
    private Context context;
    public RBNGridAdapter(Context context, RBNGridwallListResponseModel itemList) {
        this.itemList = itemList;
        this.context = context;
    }
    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_item, null);

        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);

        return rcv;
    }
    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        holder.menuName.setText(itemList.output.object.records.get(position).productName);
        //   holder.menuImage.setImageBitmap(ApplicationUtil.roundCornerImage(BitmapFactory.decodeResource(context.getResources(), itemList.get(position).getPhoto()), 50));
        ImageFetcher imageFetcher = PDHImageLoader.getImageFetcher(context);
        holder.menuImage.setTag("MenuImage");
        imageFetcher.attachImage("http://cdn.rentbean.com/"+itemList.output.object.records.get(position).medImage,  holder.menuImage, null, 0, this);
        holder.menuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, RBNPDPActivity.class);
                i.putExtra("SEO_NAME", itemList.output.object.records.get(position).seoName);
                context.startActivity(i);
            }
        });

    }
    @Override
    public int getItemCount() {
        if(itemList!=null && itemList.output!=null && itemList.output.object!=null){
            return this.itemList.output.object.records.size();
        }
        return 0;
    }

    @Override
    public void onFetchAdded(ImageView imageView, String imgUrl) {
        if(imageView.getTag().equals("MenuImage")) {
            imageView.setImageResource(R.mipmap.rent_default);
        }
    }

    @Override
    public void onFetchProgressChanged(ImageView imageView, String imgUrl, int kBTotal, int kBReceived) {

    }

    @Override
    public void onFetchFailed(ImageView imageView, String imgUrl, Exception e) {

    }

    @Override
    public void onFetchCompleted(ImageView imageView, String imgUrl, Bitmap bm) {

    }
}