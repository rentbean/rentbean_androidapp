package com.food.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.food.R;
import com.food.activity.HomeActivity;
import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.core.utils.PDHImageLoader;
import com.food.rbnGridview.model.RBNGridwallListResponseModel;

import org.droidparts.net.image.ImageFetcher;

import java.util.HashMap;
import java.util.List;

/**
 * Created by alok on 26/09/16.
 */
public class RBNGridFilterAdapter extends RecyclerView.Adapter<RBNGridFilterAdapter.RBNFilterRecyclerViewHolders>{
    private HashMap<String, String> itemList;
    private Context context;
    public RBNGridFilterAdapter(Context context, HashMap<String, String> itemList) {
        this.itemList = itemList;
        this.context = context;
    }
    @Override
    public RBNFilterRecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_rbn_filter_view_holder, null);
        RBNFilterRecyclerViewHolders rcv = new RBNFilterRecyclerViewHolders(layoutView);
        return rcv;
    }
    @Override
    public void onBindViewHolder(RBNFilterRecyclerViewHolders holder, int position) {
        Object[] arr = itemList.keySet().toArray();
        holder.radioBtn.setText(itemList.get((String)arr[position]).toString());
    }
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RBNFilterRecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{
        public RadioButton radioBtn;
        private Context context;

        public RBNFilterRecyclerViewHolders(View itemView) {
            super(itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);
            radioBtn = (RadioButton) itemView.findViewById(R.id.radioBtn);

        }
        @Override
        public void onClick(View view) {
            if(context instanceof HomeActivity) {
                ((HomeActivity) context).moveMenuScreen(getPosition());
            }
            Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }

}
