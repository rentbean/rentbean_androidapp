package com.food.cart.model;

import com.food.core.service.PDHAPIDataModelBase;

/**
 * Created by agu186 on 2/20/2016.
 */
public class AddItemToCartRequestModel extends PDHAPIDataModelBase {
    private String uid;
    private String menu_id;
    private String quantity;
    private String size;
    private String price;


    public AddItemToCartRequestModel(String uid, String menu_id,  String quantity, String size, String price) {
        this.uid = uid;
        this.menu_id = menu_id;
        this.quantity = quantity;
        this.size = size;
        this.price = price;
    }
}
