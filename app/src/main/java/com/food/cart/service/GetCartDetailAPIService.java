package com.food.cart.service;

import com.food.cart.model.EditItemToCartRequestModel;
import com.food.cart.model.EditItemToCartResponseModel;
import com.food.cart.model.GetCartDetailRequestModel;
import com.food.cart.model.GetCartDetailResponseModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by agu186 on 3/23/2016.
 */
public interface GetCartDetailAPIService {

    @Headers({
            "Accept: application/json",
            "Host: bemisaal.in"
    })
    @POST("/restAppNew/api/cart/get.json")
    Call<GetCartDetailResponseModel> getCartDetail(@Body GetCartDetailRequestModel loginRequestModel);
}
