package com.food.searchFood.provider;


import android.util.Log;

import com.food.R;
import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.categoryMenuList.model.MenuListResponseModel;
import com.food.categoryMenuList.service.CategoryListAPIService;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.login.model.LoginRequestModel;
import com.food.registration.provider.RegistrationProvider;
import com.food.searchFood.model.SearchFoodModel;
import com.food.searchFood.service.SearchFoodAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SearchFoodProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private String selectedMtn = null;
    private Boolean loanInfoRequired = null;
    private Boolean fullInfoRequired = null;

    public String jsonName = null;
    private String searchStr;

    public SearchFoodProvider(String searchStr) {
        this.searchStr = searchStr;
    }

    @Override
    public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                //.client(PDHHTTPClientFactory.getVZWHTTPClientReadCookiesFromHeader(SearchFoodProvider.lastRequestSuccess, false))
                .build();
        SearchFoodAPIService service = retrofit.create(SearchFoodAPIService.class);
        Call<MenuListResponseModel> call = service.getSearchAPI(searchStr);
        call.enqueue(new Callback<MenuListResponseModel>() {
            @Override
            public void onResponse(Response<MenuListResponseModel> response, Retrofit var) {
                MenuListResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());
                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                SearchFoodProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            SearchFoodModel model = gson.fromJson(bufferString, SearchFoodModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
