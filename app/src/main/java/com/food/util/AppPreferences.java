package com.food.util;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
/**
 *
 * Author by hemendra.kumar on 2/22/2016.
 * Class which has Utility methods
 * 
 */
public class AppPreferences {


    public static void savePreference(SharedPreferences prefs, String key,String value) {
        Editor e = prefs.edit();
        e.putString(key, value);
        e.commit();
    }

    public static void savePreference(SharedPreferences prefs, String key, boolean value) {
        Editor e = prefs.edit();
        e.putBoolean(key, value);
        e.commit();
    }



    //getting LOGIN_STATUS
    public static Boolean getLoginStatus(SharedPreferences prefs) {
        return prefs.getBoolean(ApplicationConstants.LOGIN_STATUS, false);

    }

    //saving LOGIN_STATUS
    public static void saveLoginStatus(SharedPreferences prefs,boolean value) {
        AppPreferences.savePreference(prefs, ApplicationConstants.LOGIN_STATUS, value);
    }

    //getting LOGIN_STATUS
    public static String getUserName(SharedPreferences prefs) {
        return prefs.getString(ApplicationConstants.LOGIN_USER_NAME, "");

    }

    //saving LOGIN_STATUS
    public static void saveUserName(SharedPreferences prefs,String value) {
        AppPreferences.savePreference(prefs, ApplicationConstants.LOGIN_USER_NAME, value);
    }

    //getting LOGIN_STATUS
    public static String getUserEmail(SharedPreferences prefs) {
        return prefs.getString(ApplicationConstants.LOGIN_USER_EMAIL, "");

    }

    //saving LOGIN_STATUS
    public static void saveUserEmail(SharedPreferences prefs, String value) {
        AppPreferences.savePreference(prefs, ApplicationConstants.LOGIN_USER_EMAIL, value);
    }


}
