package com.food.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import com.food.R;


/**
 * Download and set Image in {@link ImageView}.
 */
public class ImageLoader
{
	//the simplest in-memory cache implementation. This should be replaced with something like SoftReference or BitmapOptions.inPurgeable(since 1.6)
	private HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();
	private File childCacheDir;
	private int defaultImageId = R.mipmap.no_image;
	private int noImageFoundId = R.mipmap.no_image
			;
	private boolean isDirectoryStructureCreated = false;
	private boolean scaleToSmallSize = true;
	private boolean isRounded = false;
	private Context context;
	final int REQUIRED_SIZE = 60;

	
	public ImageLoader(Context context, String childDirName)
	{
		this.context = context;
	
		try
		{
			//Make the background thead low priority. This way it will not affect the UI performance
			photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);

			makeDirForCaching(context, childDirName);
		}
		catch (Exception e)
		{
			//IceBreakerUtil.log(e);
		}
		
		//sure memory before image loading.
		System.gc();
	}

	public ImageLoader(Context context)
	{
		this.context = context;
	/*	try
		{
			photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);

			makeDirForCaching(context, childDirName);
		}
		catch (Exception e)
		{
			//IceBreakerUtil.log(e);
		}*/
		
		//sure memory before image loading.
		//System.gc();
	}
	public ImageLoader(Context context, String childDirName, int defaultImageId)
	{
		this(context, childDirName);
		this.defaultImageId = defaultImageId;
	}

	public ImageLoader(Context context, String childDirName, int defaultImageId, int noImageFoundId)
	{
		this(context, childDirName);
		this.defaultImageId = defaultImageId;
		this.noImageFoundId = noImageFoundId;
	}

	/**
	 * @author vivek.srivastava
	 * call this method if you want to donot scale down.
	 */
	public void setScaleToSmallSizeFalse()
	{
		scaleToSmallSize = false;
	}

	/**
	 * @author vivek.srivastava
	 * set image rounded.
	 */
	public void setRounded(boolean isRounded)
	{
		this.isRounded = isRounded;
	}

	/**
	 * @author vivek.srivastava
	 * @return rounded image.
	 */
	
	public  Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels, boolean flag)
	{
		Bitmap output = null;
		
		try
		{
			if (bitmap != null)
			{
				if(bitmap.getHeight() >50)
				{
					pixels = pixels + 8;
				}
				else if(bitmap.getHeight() >99)
				{
					pixels = pixels + 10;
				}
				
				//Log.e(IceBreakerConstant.LOG_TAG, "getRoundedCornerBitmap bitmap.getHeight: "+bitmap.getWidth() + " " + bitmap.getHeight());
				output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);

				
				Canvas canvas = new Canvas(output);

				final int color = 0xffffffff;
				final Paint paint = new Paint();
				final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
				final RectF rectF = new RectF(rect);
				final float roundPx = pixels;

				paint.setAntiAlias(true);
				
				
				canvas.drawARGB(0, 0, 0, 0);
				paint.setColor(color);
				if(!flag){
				//canvas.drawRoundRect(rectF,UiUtils.GetDipsFromPixel(18, context),UiUtils.GetDipsFromPixel(7, context),paint);
			     canvas.drawRoundRect(rectF,(int)(18*UiUtils.getDeviceResolution(context)),(int)(7*UiUtils.getDeviceResolution(context)),paint);	
				}else
			    //canvas.drawRoundRect(rectF,UiUtils.GetDipsFromPixel(30, context),UiUtils.GetDipsFromPixel(25, context),paint);	
				 canvas.drawRoundRect(rectF,(int)(30*UiUtils.getDeviceResolution(context)),(int)(25*UiUtils.getDeviceResolution(context)),paint);	
				//canvas.drawRoundRect(rectF, 30, 10, paint);
				paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
				canvas.drawBitmap(bitmap, rect, rect, paint);
			}
		}
		catch (Exception e)
		{
			Log.e("Error Image Loader", "error to create rounded bitmap");
			e.printStackTrace();
		}

		return output;
	}
	
	
	
//	public Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels)
//	{
//		Bitmap output = null;
//
//		try
//		{
//			if (bitmap != null)
//			{
//				if(bitmap.getHeight() >50)
//				{
//					pixels = pixels + 8;
//				}
//				else if(bitmap.getHeight() >99)
//				{
//					pixels = pixels + 10;
//				}
//				//Log.e(IceBreakerConstant.LOG_TAG, "getRoundedCornerBitmap bitmap.getHeight: "+bitmap.getWidth() + " " + bitmap.getHeight());
//				output = Bitmap.createBitmap(bitmap.getWidth()+2, bitmap.getHeight()+2, Config.ARGB_8888);
//
//
//				Canvas canvas = new Canvas(output);
//
//				int color = 0xff8d8d8d;
//				final Paint paint = new Paint();
//				Rect rect = new Rect(0, 0, bitmap.getWidth()+2, bitmap.getHeight()+2);
//				RectF rectF = new RectF(rect);
//				final float roundPx = pixels;
//
//				paint.setAntiAlias(true);
//				
//				canvas.drawARGB(0, 0, 0, 0);
//			    paint.setColor(color);
//			    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
//			    
//			    rect = new Rect(1, 1, bitmap.getWidth() + 1, bitmap.getHeight() +1);
//			    rectF = new RectF(rect);
//			    color = 0xff8d8d8d;
//				paint.setColor(color);
//				canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
//				paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
//				canvas.drawBitmap(bitmap, 1, 1, paint);
//			}
//		}
//		catch (Exception e)
//		{
//			Log.e(IceBreakerConstant.LOG_TAG, "error to create rounded bitmap");
//			IceBreakerUtil.log(e);
//		}
//
//		return output;
//	}
	
//	private float dpFromPx(float px)
//	{
//	    return px / context.getResources().getDisplayMetrics().density;
//	}


	/**
	 * Method makes directories for
	 * files caching on device sd card.
	 */
	private void makeDirForCaching(Context context, String childDirName)
	{
		File appCacheDirectory = null;
		boolean isDirectoryStructureCreated = false;

		//SD Card
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			isDirectoryStructureCreated = createDirectoryStructure(context.getExternalCacheDir(), childDirName);
		}

		if (!isDirectoryStructureCreated)
		{
			//Phone Memory
			appCacheDirectory = context.getCacheDir();
			isDirectoryStructureCreated = createDirectoryStructure(appCacheDirectory, childDirName);
		}

		this.isDirectoryStructureCreated = isDirectoryStructureCreated;

	}

	private boolean createDirectoryStructure(File rootLocation, String childDirName)
	{
		boolean isDirectoryStructureCreated = true;
		File parentCacheDir = null;
		try
		{
			parentCacheDir = new File(rootLocation, "IceBreaker");
			childCacheDir = new File(parentCacheDir, childDirName);

			if (!childCacheDir.exists())
			{
				childCacheDir.mkdirs();
			}
		}
		catch (Exception e)
		{
			isDirectoryStructureCreated = false;
			//Consume it
		}

		return isDirectoryStructureCreated;
	}

	public Bitmap getImageFromSdCard(String url)
	{
//		File file = null;
//		
//		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
//		{
//			file = context.getExternalCacheDir();
//		}
//		if(file == null)
//		{
//			file = context.getCacheDir();
//		}
		
		String filename = String.valueOf(url.hashCode());
		File f = new File(childCacheDir, filename);
	
		return decodeFile(f);
	}
	/**
	 * set image in {@link ImageView}.
	 * @param url is the URL of the image.
	 * @param imageView is the reference of {@link ImageView}.
	 */
	public void displayImage(String url, ImageView imageView)
	{
		try
		{
			if (url != null && !url.equals(""))
			{
				Bitmap bitmap = cache.get(url);//search in hash map

				//Bitmap bitmap = null;
				//search in sd card
				boolean isRoundedBitmapExistOnCache = true;
				if (bitmap == null)
				{
					isRoundedBitmapExistOnCache = false;
//					String filename = String.valueOf(url.hashCode());
//					String imageInSD = Environment.getExternalStorageDirectory() + "/" + LOG_TAG + "/" + LOG_TAG + "/" + filename;
//					//bitmap = BitmapFactory.decodeFile(imageInSD);
//					bitmap = decodeFile(new File(imageInSD));
					bitmap = getImageFromSdCard(url);
					Log.i("Image Loader", "bitmap: " + bitmap);
				}

				if (bitmap != null)
				{
					if (isRounded && !isRoundedBitmapExistOnCache)
					{
						bitmap = getRoundedCornerBitmap(bitmap, 10,false);
						cache.put(url, bitmap);
						//imageView.setImageBitmap(bitmap);
						imageView.setBackgroundDrawable(new BitmapDrawable(bitmap));
					}
					else
					{
						//imageView.setImageBitmap(bitmap);
						imageView.setBackgroundDrawable(new BitmapDrawable(bitmap));
					}
				}
				else
				{
					queuePhoto(url, imageView);

					try
					{
						Resources res = this.context.getResources();
						Bitmap defaultImage = BitmapFactory.decodeResource(res, defaultImageId);

						//imageView.setImageResource(defaultImageId);
						if (isRounded)
						{
							//imageView.setImageBitmap(getRoundedCornerBitmap(defaultImage, 10,false));
							imageView.setBackgroundDrawable(new BitmapDrawable(getRoundedCornerBitmap(defaultImage, 10, false)));
						}
						else
						{
							//imageView.setImageBitmap(defaultImage);
							imageView.setBackgroundDrawable(new BitmapDrawable(defaultImage));
						}
					}
					catch (Exception e)
					{
						//consume no resource found exception
					}
				}
			}
			else
			{
				Resources res = this.context.getResources();
				Bitmap defaultImage = BitmapFactory.decodeResource(res, defaultImageId);
				//imageView.setImageResource(defaultImageId);
				if (isRounded)
				{
					//imageView.setImageBitmap(getRoundedCornerBitmap(defaultImage, 10,false));
					imageView.setBackgroundDrawable(new BitmapDrawable(getRoundedCornerBitmap(defaultImage, 10, false)));
				}
				else
				{
					//imageView.setImageBitmap(defaultImage);
					imageView.setBackgroundDrawable(new BitmapDrawable(defaultImage));
				}
			}
		}
		catch (Exception e)
		{

		}
	}

	private void queuePhoto(String url, ImageView imageView)
	{
		try
		{
			//This ImageView may be used for other images before. So there may be some old tasks in the queue. We need to discard them. 
			photosQueue.Clean(imageView);
			PhotoToLoad p = new PhotoToLoad(url, imageView);

			synchronized (photosQueue.photosToLoad)
			{
				photosQueue.enqueue(p);
				photosQueue.photosToLoad.notifyAll();
			}

			//start thread if it's not started yet
			if (photoLoaderThread.getState() == Thread.State.NEW)
				photoLoaderThread.start();
		}
		catch (Exception e)
		{
			//IceBreakerUtil.log(e);
		}
	}

	private Bitmap getBitmap(String url)
	{
		//from web
		Bitmap bitmap = null;
		try
		{
			if (!isDirectoryStructureCreated)
			{
				bitmap = ImageOperations(url);
				return bitmap;
			}

			String filename = String.valueOf(url.hashCode());
			File f = new File(childCacheDir, filename);

			//from SD cache
			bitmap = decodeFile(f);
			if (bitmap != null)
			{
				return bitmap;
			}

			InputStream is = new URL(url).openStream();
			OutputStream os = new FileOutputStream(f);
			CopyStream(is, os);
			os.close();
			bitmap = decodeFile(f);
			return bitmap;
		}
		catch (Exception ex)
		{
			//IceBreakerUtil.log(ex);
			return null;
		}
	}

	/**
	 * decodes image and scales it to reduce memory consumption
	 */
	public Bitmap decodeFile(File f)
	{
		try
		{
			//decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);
			
			//Find the correct scale value. It should be the power of 2.
			
			//int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			
			if(scaleToSmallSize)
			{
				scale = calculateInSampleSize(o, REQUIRED_SIZE, REQUIRED_SIZE);
			}
			else
			{
				Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
				int width = display.getWidth();
				int height = display.getHeight();
				scale = calculateInSampleSize(o, width, height);
			}

			//decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inJustDecodeBounds = false;
			//Log.e(IceBreakerConstant.LOG_TAG, "before decodeStream....");
			
			return BitmapFactory.decodeStream(new FileInputStream(f),null,o2);
		}
		catch (FileNotFoundException e)
		{
			//Log.i(IceBreakerConstant.LOG_TAG, "FileNotFoundException");
		}
		catch (Throwable e) { 
    		// TODO Auto-generated catch block 
    		e.printStackTrace(); 
    		};
		return null;
	}

	//Task for the queue
	private class PhotoToLoad
	{
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i)
		{
			url = u;
			imageView = i;
		}
	}

	PhotosQueue photosQueue = new PhotosQueue();

	public void stopThread()
	{
		try
		{
			photoLoaderThread.interrupt();
		}
		catch (Exception e)
		{
			//IceBreakerUtil.log(e);
		}
	}

	public class PhotosQueue
	{
		private LinkedList<PhotoToLoad> photosToLoad = new LinkedList<PhotoToLoad>();

		public boolean isEmpty()// Post: Returns true if the queue is empty. Otherwise, false.
		{
			return (photosToLoad.size() == 0);
		}

		public void enqueue(Object item)// Post: An item is added to the back of the queue.
		{
			// Append the item to the end of our linked list.
			photosToLoad.add((PhotoToLoad) item);
		}

		/**
		 * Pre: this.isEmpty() == false
		 * Post: The item at the front of the queue is returned and 
		 * deleted from the queue. Returns null if precondition
		 * not met.
		 * @return
		 */
		public Object dequeue()
		{
			Object item = null;
			try
			{
				// Store a reference to the item at the front of the queue
				//   so that it does not get garbage collected when we 
				//   remove it from the list.
				// Note: list.get(...) returns null if item not found at
				//   specified index. See postcondition.
				item = photosToLoad.get(0);
				// Remove the item from the list.
				// My implementation of the linked list is based on the
				//   J2SE API reference. In both, elements start at 1,
				//   unlike arrays which start at 0.
				photosToLoad.remove(0);

				// Return the item
			}
			catch (Exception e)
			{
				//IceBreakerUtil.log(e);
			}

			return item;
		}

		/**
		 * Pre: this.isEmpty() == false Post: The item at the front of the queue is returned and 
		 * deleted from the queue. Returns null if precondition not met.
		 * @return
		 */
		public Object peek()
		{
			try
			{
				// This method is very similar to dequeue().
				// See Queue.dequeue() for comments.
				return photosToLoad.get(0);
			}
			catch (Exception e)
			{
				//IceBreakerUtil.log(e);
			}
			return null;
		}

		//removes all instances of this ImageView
		public void Clean(ImageView image)
		{
			try
			{
				for (int j = 0; j < photosToLoad.size();)
				{
					if (photosToLoad.get(j).imageView == image)
					{

						photosToLoad.remove(j);
					}
					else
					{
						++j;
					}
				}
			}
			catch (Exception e)
			{
				//IceBreakerUtil.log(e);
				// index outofbound exception
			}
		}
	}

	//stores list of photos to download
	/*
	 * class PhotosQueue {
	 * 
	 * private Stack<PhotoToLoad> photosToLoad1= new Stack<PhotoToLoad>();
	 * 
	 * 
	 * //removes all instances of this ImageView public void Clean(ImageView
	 * image) {
	 * 
	 * for(int j=0 ;j<photosToLoad.size();) { if(photosToLoad.get(j).imageView
	 * == image) { photosToLoad.remove(j); } else { ++j; } } } }
	 */

	class PhotosLoader extends Thread
	{
		public void run()
		{
			try
			{
				while (true)
				{
					//thread waits until there are any images to load in the queue
					if (photosQueue.photosToLoad.size() == 0)
						synchronized (photosQueue.photosToLoad)
						{
							photosQueue.photosToLoad.wait();
						}
					if (photosQueue.photosToLoad.size() != 0)
					{
						PhotoToLoad photoToLoad;
						synchronized (photosQueue.photosToLoad)
						{
							//photoToLoad=photosQueue.photosToLoad.pop();

							photoToLoad = (PhotoToLoad) photosQueue.dequeue();
						}

						Bitmap bmp = getBitmap(photoToLoad.url);

						if (bmp != null)
						{
							if (isRounded)
							{
								bmp = getRoundedCornerBitmap(bmp, 10, false);
							}
							
							cache.put(photoToLoad.url, bmp);
						}

						Object tag = photoToLoad.imageView.getTag();
						if (tag != null && ((String) tag).equals(photoToLoad.url))
						{
							BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad.imageView, photoToLoad.url);
							Activity a = (Activity) photoToLoad.imageView.getContext();
							a.runOnUiThread(bd);
						}
					}
					if (Thread.interrupted())
						break;
				}
			}
			catch (InterruptedException e)
			{
				//IceBreakerUtil.log(e);
				//allow thread to exit
			}
		}
	}

	PhotosLoader photoLoaderThread = new PhotosLoader();

	//Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable
	{
		Bitmap bitmap;
		ImageView imageView;
		private String url;

		public BitmapDisplayer(Bitmap b, ImageView i, String url)
		{
			bitmap = b;
			imageView = i;
			this.url = url;
		}

		public void run()
		{
			try
			{
				if (bitmap != null)
				{
					
					if (isRounded)
					{
						bitmap = getRoundedCornerBitmap(bitmap, 10, false);
						cache.put(url, bitmap);
						//imageView.setImageBitmap(bitmap);
						imageView.setBackgroundDrawable(new BitmapDrawable(bitmap));
					}
					else
					{
						//imageView.setImageBitmap(bitmap);
						imageView.setBackgroundDrawable(new BitmapDrawable(bitmap));
					}
				}
				else
				{
					Resources res = context.getResources();
					Bitmap defaultImage = BitmapFactory.decodeResource(res, noImageFoundId);
					//imageView.setImageResource(defaultImageId);
					if (isRounded)
					{
						//imageView.setImageBitmap(getRoundedCornerBitmap(defaultImage, 10,false));
						imageView.setBackgroundDrawable(new BitmapDrawable(getRoundedCornerBitmap(defaultImage, 10, false)));
					}
					else
					{
						//imageView.setImageBitmap(defaultImage);
						imageView.setBackgroundDrawable(new BitmapDrawable(defaultImage));
					}
					//imageView.setImageResource(noImageFoundId); 
				}
			}
			catch (Exception e)
			{
				//IceBr`eakerUtil.log(e);
			}
		}
	}

	/**
	 * delete cache image from sd card.
	 */
	public void clearCache()
	{
		//clear memory cache
		clearLocalCache();

		//clear SD cache
		File[] files = null;

		try
		{
			files = childCacheDir.listFiles();

			if (files != null)
			{
				for (File file : files)
				{
					deleteFile(file);
				}
			}
		}
		catch (Exception e)
		{
			//Consume it
		}
	}
	
	public void clearLocalCache()
	{
		try
		{
			if(cache != null)
			{
				Set<String> set = cache.keySet();

				for(String key : set)
				{
					Bitmap bitmap = cache.get(key);
					bitmap.recycle();
					bitmap = null;
				}
				//clear memory cache
				cache.clear();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	private void deleteFile(File file)
	{
		try
		{
			file.delete();
		}
		catch (Exception exception)
		{
			//consume it
		}
	}

	private void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size = 1024;
		try
		{
			byte[] bytes = new byte[buffer_size];
			for (;;)
			{
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
				{
					break;
				}
				os.write(bytes, 0, count);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	private Bitmap ImageOperations(String url)
	{
		Bitmap bmp = null;
		try
		{
			InputStream is = (InputStream) fetch(url);
			bmp = BitmapFactory.decodeStream(is);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		finally
		{

		}
		return bmp;
	}

	private Object fetch(String address) throws MalformedURLException, IOException, FileNotFoundException, UnknownHostException, Exception
	{
		Object content = null;

		URL url = new URL(address);
		URLConnection connection = url.openConnection();
		connection.setReadTimeout(60000);
		connection.setConnectTimeout(120000);
		
		content = connection.getContent();

		return content;
	}
	
	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth)
		{
			if (width > height)
			{
				inSampleSize = Math.round((float) height / (float) reqHeight);
			}
			else
			{
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
		
		return inSampleSize;
	}
}
