package com.food.rbnSorting.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public class RBNGridSortingRequestModel extends PDHAPIDataModelBase {

@SerializedName("output")
@Expose
public Output output;

    @SerializedName("errorMap")
    @Expose
    public ErrorMap errorMap;

    @SerializedName("serviceCode")
    @Expose
    public String serviceCode;

    @SerializedName("serviceMessage")
    @Expose
    public String serviceMessage;



public class Output {

    @SerializedName("object")
    @Expose
    public Object object;

    public class Object{

        @SerializedName("product")
        @Expose
        public Product prodcuct;

        @SerializedName("skus")
        @Expose
        public ArrayList<Skus> skus;

        public class Skus {
            @SerializedName("id")
            @Expose
            public String id;

            @SerializedName("skuid")
            @Expose
            public String skuid;

            @SerializedName("name")
            @Expose
            public String name;

            @SerializedName("description")
            @Expose
            public String description;

            @SerializedName("skuImage")
            @Expose
            public ArrayList<SkuImage> skuImage;


            @SerializedName("status")
            @Expose
            public int status;

            @SerializedName("minTenure")
            @Expose
            public int minTenure;

            @SerializedName("displayMinTenure")
            @Expose
            public String displayMinTenure;

            @SerializedName("maxTenure")
            @Expose
            public int maxTenure;

            @SerializedName("displayMaxTenure")
            @Expose
            public String displayMaxTenure;


            public class SkuImage {
                @SerializedName("id")
                @Expose
                public String id;

                @SerializedName("imageId")
                @Expose
                public String imageId;

                @SerializedName("thumbnailImage")
                @Expose
                public String thumbnailImage;

                @SerializedName("smallImage")
                @Expose
                public String smallImage;

                @SerializedName("medImage")
                @Expose
                public String medImage;

                @SerializedName("largeImage")
                @Expose
                public String largeImage;



            }


        }



        public class Product {
            @SerializedName("id")
            @Expose
            public String id;

            @SerializedName("brand")
            @Expose
            public String brand;

            @SerializedName("productid")
            @Expose
            public String productid;

            @SerializedName("name")
            @Expose
            public String name;

            @SerializedName("description")
            @Expose
            public String description;

            @SerializedName("productImage")
            @Expose
            public String productImage;

            @SerializedName("imageId")
            @Expose
            public String imageId;

            @SerializedName("parentCategoryId")
            @Expose
            public String parentCategoryId;

          /*  @SerializedName("skus")
            @Expose
            public ArrayList<Map<Integer,String> > sk;*/

            @SerializedName("aggregateRating")
            @Expose
            public float aggregateRating;

            @SerializedName("numOfReviews")
            @Expose
            public int numOfReviews;

            @SerializedName("locations")
            @Expose
            public ArrayList<String> locations;

            @SerializedName("recommendedProducts")
            @Expose
            public String recommendedProducts;

            @SerializedName("seoName")
            @Expose
            public String seoName;

            @SerializedName("features")
            @Expose
            public Features features;


            @SerializedName("status")
            @Expose
            public int status;

            @SerializedName("keywords")
            @Expose
            public String keywords;

            @SerializedName("acceptBackorder")
            @Expose
            public boolean acceptBackorder;

            @SerializedName("maxBackorderCount")
            @Expose
            public int maxBackorderCount;

            @SerializedName("procurementDays")
            @Expose
            public int procurementDays;




            public class Features {
                @SerializedName("ideal for")
                @Expose
                public String idealFor;

                @SerializedName("Weight")
                @Expose
                public String Weight;

                @SerializedName("Charging Source")
                @Expose
                public String ChargingSource;

                @SerializedName("Charging Time")
                @Expose
                public String ChargingTime;

                @SerializedName("Utility Time")
                @Expose
                public String UtilityTime ;

                @SerializedName("Speed")
                @Expose
                public String Speed;

                @SerializedName("Color")
                @Expose
                public String Color;


            }

        }
    }

    }

    public class ErrorMap {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }


}
