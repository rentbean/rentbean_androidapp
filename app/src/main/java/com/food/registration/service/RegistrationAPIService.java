/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.registration.service;

import com.food.registration.model.RegistrationRequestModel;
import com.food.registration.model.RegistrationResponseModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by shaimu8 on 12/15/15.
 */
public interface RegistrationAPIService {
    @Headers({
            "Accept: application/json",
            "Host: bemisaal.in"
    })
    @POST("/restAppNew/api/siteUsers/registeration.json")
    Call<RegistrationResponseModel> getRegistration(@Body RegistrationRequestModel regisRequestModel);
}
